<?php

App::uses('AppModel', 'Model');

/**
 * IncidentUpdate Model
 *
 * @property IncidentType $IncidentType
 * @property Severity $Severity
 * @property IncidentStatus $IncidentStatus
 * @property Technician $Technician
 * @property User $User
 * @property Incident $Incident
 */
class IncidentUpdate extends AppModel {

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'description';

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'description' => array(
            'notEmpty' => array(
                'rule' => array('notEmpty'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'incident_type_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'severity_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'incident_status_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'technician_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'user_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'authorised_by' => array(
            'notEmpty' => array(
                'rule' => array('notEmpty'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'incident_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'IncidentType' => array(
            'className' => 'IncidentType',
            'foreignKey' => 'incident_type_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Severity' => array(
            'className' => 'Severity',
            'foreignKey' => 'severity_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'IncidentStatus' => array(
            'className' => 'IncidentStatus',
            'foreignKey' => 'incident_status_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Technician' => array(
            'className' => 'Technician',
            'foreignKey' => 'technician_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Incident' => array(
            'className' => 'Incident',
            'foreignKey' => 'incident_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

}
