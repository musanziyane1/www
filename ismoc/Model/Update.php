<?php

App::uses('AppModel', 'Model');

/**
 * Update Model
 *
 * @property Incident $Incident
 */
class Update extends AppModel {

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'description';


    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'Incident' => array(
            'className' => 'Incident',
            'foreignKey' => 'incident_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

}
