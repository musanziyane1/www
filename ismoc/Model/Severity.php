<?php

App::uses('AppModel', 'Model');

/**
 * Severity Model
 *
 * @property Incident $Incident
 */
class Severity extends AppModel {

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'description';


    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * hasMany associations
     *
     * @var array
     */
    public $hasMany = array(
        'Incident' => array(
            'className' => 'Incident',
            'foreignKey' => 'severity_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

}
