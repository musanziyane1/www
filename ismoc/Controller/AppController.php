<?php

/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

    public $components = array(
        //'DebugKit.Toolbar',
        'Session',
        'Cookie',
        'RequestHandler',
        'Auth' => array(
            'loginRedirect' => array('controller' => 'sims', 'action' => 'dashboard'),
            'logoutRedirect' => array('controller' => 'users', 'action' => 'login'),
            'authError' => 'You must be logged in to view this page.',
            'loginError' => 'Invalid Username or Password entered, please try again.'
    ));

// only allow the login controllers only
    public function beforeFilter() {
        //$this->Auth->allow('login');

        parent::beforeFilter();
        $this->Auth->allow(array('controller' => 'notifications', 'action' => 'add'));

        //AuthComponent::user('id');
        $this->Cookie->name = 'iSMOCCookie';
        $this->Cookie->time = '1209600';
        $this->Cookie->domain = 'http://54.225.82.1/';
        $this->Cookie->key = 'fdshj453*&$bkhbkbhKJDHHG##HLDKJ@*ddfskq23484234';
        $this->Cookie->httpOnly = true;

        if (!$this->Auth->loggedIn() && $this->Cookie->read('iSMOCCookie')) {
            $cookie = $this->Cookie->read('iSMOCCookie');
            //$this->log($cookie, 'debug');
            $this->loadModel('User');
            $user = $this->User->find('first', array(
                'conditions' => array(
                    'User.email' => $cookie['User']['email'],
                    'User.password' => $cookie['User']['password']
                ),
                'contain' => true,
                'fields' => array('User.email', 'User.password', 'User.first_name', 'User.last_name')
                    )
            );
            if ($user) {
                if (!$this->Auth->login($user['User'])) {
                    $this->redirect('/users/logout');
                }
            }
        }
    }

}
