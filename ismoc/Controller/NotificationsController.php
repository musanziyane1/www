<?php

App::uses('AppController', 'Controller');

/**
 * Posts Controller
 *
 * @property Post $Post
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class NotificationsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session', 'RequestHandler');

    public function index() {
        $this->layout = false;

        $notifications = $this->Notification->find('all');
        $this->set(array(
            'notifications' => $notifications,
            '_serialize' => array('notifications')
        ));
    }

    public function view($id) {
        $notification = $this->Notification->findById($id);
        $this->set(array(
            'notification' => $notification,
            '_serialize' => array('notification')
        ));
    }

    public function add() {

        $this->layout = false;


        //$parameters = $this->request->data['SIMInfo'];
        //Debugger::log($parameters, $level = 7, $depth = 3);
        //$this->Notification->id = $id;
        if ($this->Notification->save($this->request->data)) {
            $message = array(
                'text' => __('200'),
                'type' => 'success'
            );
        } else {
            $message = array(
                'text' => __('100'),
                'type' => 'error'
            );
        }
        $this->set(array(
            'message' => $message,
            '_serialize' => array('message')
        ));
    }

    public function edit($id) {
        $this->Notification->id = $id;
        if ($this->Notification->save($this->request->data)) {
            $message = array(
                'text' => __('Saved'),
                'type' => 'success'
            );
        } else {
            $message = array(
                'text' => __('Error'),
                'type' => 'error'
            );
        }
        $this->set(array(
            'message' => $message,
            '_serialize' => array('message')
        ));
    }

    public function delete($id) {
        if ($this->Notification->delete($id)) {
            $message = array(
                'text' => __('Deleted'),
                'type' => 'success'
            );
        } else {
            $message = array(
                'text' => __('Error'),
                'type' => 'error'
            );
        }
        $this->set(array(
            'message' => $message,
            '_serialize' => array('message')
        ));
    }

}
