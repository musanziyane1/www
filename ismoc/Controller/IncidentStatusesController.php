<?php

App::uses('AppController', 'Controller');

/**
 * IncidentStatuses Controller
 *
 * @property IncidentStatus $IncidentStatus
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class IncidentStatusesController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session');

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->IncidentStatus->recursive = 0;
        $this->set('incidentStatuses', $this->Paginator->paginate());
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->IncidentStatus->exists($id)) {
            throw new NotFoundException(__('Invalid incident status'));
        }
        $options = array('conditions' => array('IncidentStatus.' . $this->IncidentStatus->primaryKey => $id));
        $this->set('incidentStatus', $this->IncidentStatus->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if ($this->request->is('post')) {
            $this->IncidentStatus->create();
            if ($this->IncidentStatus->save($this->request->data)) {
                $this->Session->setFlash(__('The incident status has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The incident status could not be saved. Please, try again.'));
            }
        }
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->IncidentStatus->exists($id)) {
            throw new NotFoundException(__('Invalid incident status'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->IncidentStatus->save($this->request->data)) {
                $this->Session->setFlash(__('The incident status has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The incident status could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('IncidentStatus.' . $this->IncidentStatus->primaryKey => $id));
            $this->request->data = $this->IncidentStatus->find('first', $options);
        }
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->IncidentStatus->id = $id;
        if (!$this->IncidentStatus->exists()) {
            throw new NotFoundException(__('Invalid incident status'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->IncidentStatus->delete()) {
            $this->Session->setFlash(__('The incident status has been deleted.'));
        } else {
            $this->Session->setFlash(__('The incident status could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

}
