<?php

App::uses('AppController', 'Controller');

/**
 * IncidentUpdates Controller
 *
 * @property IncidentUpdate $IncidentUpdate
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class IncidentUpdatesController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session');

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->IncidentUpdate->recursive = 0;
        $this->set('incidentUpdates', $this->Paginator->paginate());
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->IncidentUpdate->exists($id)) {
            throw new NotFoundException(__('Invalid incident update'));
        }
        $options = array('conditions' => array('IncidentUpdate.' . $this->IncidentUpdate->primaryKey => $id));
        $this->set('incidentUpdate', $this->IncidentUpdate->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if ($this->request->is('post')) {
            $this->IncidentUpdate->create();
            if ($this->IncidentUpdate->save($this->request->data)) {
                $this->Session->setFlash(__('The incident update has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The incident update could not be saved. Please, try again.'));
            }
        }
        $incidentTypes = $this->IncidentUpdate->IncidentType->find('list');
        $severities = $this->IncidentUpdate->Severity->find('list');
        $incidentStatuses = $this->IncidentUpdate->IncidentStatus->find('list');
        $technicians = $this->IncidentUpdate->Technician->find('list');
        $users = $this->IncidentUpdate->User->find('list');
        $incidents = $this->IncidentUpdate->Incident->find('list');
        $this->set(compact('incidentTypes', 'severities', 'incidentStatuses', 'technicians', 'users', 'incidents'));
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->IncidentUpdate->exists($id)) {
            throw new NotFoundException(__('Invalid incident update'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->IncidentUpdate->save($this->request->data)) {
                $this->Session->setFlash(__('The incident update has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The incident update could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('IncidentUpdate.' . $this->IncidentUpdate->primaryKey => $id));
            $this->request->data = $this->IncidentUpdate->find('first', $options);
        }
        $incidentTypes = $this->IncidentUpdate->IncidentType->find('list');
        $severities = $this->IncidentUpdate->Severity->find('list');
        $incidentStatuses = $this->IncidentUpdate->IncidentStatus->find('list');
        $technicians = $this->IncidentUpdate->Technician->find('list');
        $users = $this->IncidentUpdate->User->find('list');
        $incidents = $this->IncidentUpdate->Incident->find('list');
        $this->set(compact('incidentTypes', 'severities', 'incidentStatuses', 'technicians', 'users', 'incidents'));
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->IncidentUpdate->id = $id;
        if (!$this->IncidentUpdate->exists()) {
            throw new NotFoundException(__('Invalid incident update'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->IncidentUpdate->delete()) {
            $this->Session->setFlash(__('The incident update has been deleted.'));
        } else {
            $this->Session->setFlash(__('The incident update could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

}
