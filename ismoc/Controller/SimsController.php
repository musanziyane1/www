<?php

App::uses('AppController', 'Controller');

/**
 * Sims Controller
 *
 * @property Sim $Sim
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class SimsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session');
    public $uses = array('Sim', 'Incident', 'Technician', 'Customer', 'Notification');

    /**
     * index method
     *
     * @return void
     */
    public function overview() {
        $this->Sim->recursive = 0;
        $this->set('sims', $this->Paginator->paginate());
    }

    public function dashboard($searchParam = null) {
        
        $totalIncidents = $this->Incident->find('count');
        $this->set('totalIncidents', $totalIncidents);

        $incidents = $this->Incident->find('all', array('order' => 'Incident.created DESC Limit 7'));
        $this->set('incidents', $incidents);

        $openedIncidents = $this->Incident->find('count', array('conditions' => array('Incident.incident_status_id' => '1')));
        $this->set('openedIncidents', $openedIncidents);

        $inprogressIncidents = $this->Incident->find('count', array('conditions' => array('Incident.incident_status_id' => '2')));
        $this->set('inprogressIncidents', $inprogressIncidents);

        $escalatedIncidents = $this->Incident->find('count', array('conditions' => array('Incident.incident_status_id' => '5')));
        $this->set('escalatedIncidents', $escalatedIncidents);

        $closedIncidents = $this->Incident->find('count', array('conditions' => array('Incident.incident_status_id' => '6')));
        $this->set('closedIncidents', $closedIncidents);

        $totalSims = $this->Sim->find('count');
        $this->set('totalSims', $totalSims);

        $totalTechnicians = $this->Technician->find('count');
        $this->set('totalTechnicians', $totalTechnicians);


        $totalCustomers = $this->Sim->Customer->find('count');
        $this->set('totalCustomers', $totalCustomers);
        
        $this->Sim->recursive = 0;
        $this->set('sims', $this->Paginator->paginate());
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->Sim->exists($id)) {
            throw new NotFoundException(__('Invalid sim'));
        }
        $options = array('conditions' => array('Sim.' . $this->Sim->primaryKey => $id));
        $this->set('sim', $this->Sim->find('first', $options));
        
       if (!empty($this->data)) {
            $this->Incident->save($this->data);
            $this->Session->setFlash('A new category has been added');
            $this->redirect(array('action' => 'index'));
        } else {
            $parents[0] = "[Top]";
            $incidents = $this->Incident->generateTreeList(null, null, null, '&nbsp;&nbsp;&nbsp;');
            if ($incidents) {
                foreach ($incidents as $key => $value)
                    $parents[$key] = $value;
            }
            $this->set(compact('parents'));

            $incidentTypes = $this->Incident->IncidentType->find('list');
            $severities = $this->Incident->Severity->find('list');
            $incidentStatuses = $this->Incident->IncidentStatus->find('list');
            $technicians = $this->Incident->Technician->find('list');
            $users = $this->Incident->User->find('list');
            $parentIncidents = $this->Incident->ParentIncident->find('list');
            $sims = $this->Incident->Sim->find('list');
            $this->set(compact('incidentTypes', 'severities', 'incidentStatuses', 'technicians', 'users', 'parentIncidents', 'sims'));
            
            
        }
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if ($this->request->is('post')) {
            $this->Sim->create();
            if ($this->Sim->save($this->request->data)) {
                $this->Session->setFlash(__('The sim has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The sim could not be saved. Please, try again.'));
            }
        }

        $customers = $this->Sim->Customer->find('list');
        $this->set(compact('customers'));
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->Sim->exists($id)) {
            throw new NotFoundException(__('Invalid sim'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->Sim->save($this->request->data)) {
                $this->Session->setFlash(__('The sim has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The sim could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('Sim.' . $this->Sim->primaryKey => $id));
            $this->request->data = $this->Sim->find('first', $options);
        }

        $customers = $this->Sim->Customer->find('list');
        $this->set(compact('apns', 'customers', 'incidents'));
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->Sim->id = $id;
        if (!$this->Sim->exists()) {
            throw new NotFoundException(__('Invalid sim'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->Sim->delete()) {
            $this->Session->setFlash(__('The sim has been deleted.'));
        } else {
            $this->Session->setFlash(__('The sim could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

}
