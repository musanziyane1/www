<?php

App::uses('AppController', 'Controller');

/**
 * Technicians Controller
 *
 * @property Technician $Technician
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class TechniciansController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session');

    /**
     * index method
     *
     * @return void
     */
    public function overview() {
        $this->Technician->recursive = 0;
        $this->set('technicians', $this->Paginator->paginate());
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->Technician->exists($id)) {
            throw new NotFoundException(__('Invalid technician'));
        }
        $options = array('conditions' => array('Technician.' . $this->Technician->primaryKey => $id));
        $this->set('technician', $this->Technician->find('first', $options));

        if (!$this->Technician->exists($id)) {
            throw new NotFoundException(__('Invalid technician'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->Technician->save($this->request->data)) {
                $this->Session->setFlash(__('The technician has been saved.'));
                return $this->redirect(array('action' => 'overview'));
            } else {
                $this->Session->setFlash(__('The technician could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('Technician.' . $this->Technician->primaryKey => $id));
            $this->request->data = $this->Technician->find('first', $options);
        }
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if ($this->request->is('post')) {
            $this->Technician->create();
            if ($this->Technician->save($this->request->data)) {
                $this->Session->setFlash(__('The technician has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The technician could not be saved. Please, try again.'));
            }
        }
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->Technician->exists($id)) {
            throw new NotFoundException(__('Invalid technician'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->Technician->save($this->request->data)) {
                $this->Session->setFlash(__('The technician has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The technician could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('Technician.' . $this->Technician->primaryKey => $id));
            $this->request->data = $this->Technician->find('first', $options);
        }
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->Technician->id = $id;
        if (!$this->Technician->exists()) {
            throw new NotFoundException(__('Invalid technician'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->Technician->delete()) {
            $this->Session->setFlash(__('The technician has been deleted.'));
        } else {
            $this->Session->setFlash(__('The technician could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

}
