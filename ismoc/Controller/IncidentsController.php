<?php

class IncidentsController extends AppController {

    var $description = 'Incidents';
    public $components = array('Paginator', 'Session');

    public function overview() {
        $this->Incident->recursive = 0;
        $this->set('incidents', $this->Paginator->paginate());
    }

    function index() {
        $incidents = $this->Incident->generateTreeList(null, null, null, '&nbsp;&nbsp;&nbsp;');
        $this->set(compact('incidents'));
    }

    public function view($id = null) {
        if (!$this->Incident->exists($id)) {
            throw new NotFoundException(__('Invalid incident'));
        }
        $options = array('conditions' => array('Incident.' . $this->Incident->primaryKey => $id));
        $this->set('incident', $this->Incident->find('first', $options));

        if (!empty($this->data)) {
            if ($this->Incident->save($this->data) == false)
                $this->Session->setFlash('Error saving Incident.');
            $this->redirect(array('action' => 'index'));
        } else {
            if ($id == null)
                die("No ID received");
            $this->data = $this->Incident->read(null, $id);
            $parents[0] = "[ Top ]";
            $incidents = $this->Incident->generateTreeList(null, null, null, '&nbsp;&nbsp;&nbsp;');
            if ($incidents)
                foreach ($incidents as $key => $value)
                    $parents[$key] = $value;
            $this->set(compact('parents'));
        }


        $incidentTypes = $this->Incident->IncidentType->find('list');
        $severities = $this->Incident->Severity->find('list');
        $incidentStatuses = $this->Incident->IncidentStatus->find('list');
        $technicians = $this->Incident->Technician->find('list');
        $users = $this->Incident->User->find('list');
        $parentIncidents = $this->Incident->ParentIncident->find('list');
        $sims = $this->Incident->Sim->find('list');
        $this->set(compact('incidentTypes', 'severities', 'incidentStatuses', 'technicians', 'users', 'parentIncidents', 'sims'));
    }

    function add() {

        if (!empty($this->data)) {
            $this->Incident->save($this->data);
            $this->Session->setFlash('A new category has been added');
            $this->redirect(array('action' => 'index'));
        } else {
            $parents[0] = "[Top]";
            $incidents = $this->Incident->generateTreeList(null, null, null, '&nbsp;&nbsp;&nbsp;');
            if ($incidents) {
                foreach ($incidents as $key => $value)
                    $parents[$key] = $value;
            }
            $this->set(compact('parents'));

            $incidentTypes = $this->Incident->IncidentType->find('list');
            $severities = $this->Incident->Severity->find('list');
            $incidentStatuses = $this->Incident->IncidentStatus->find('list');
            $technicians = $this->Incident->Technician->find('list');
            $users = $this->Incident->User->find('list');
            $parentIncidents = $this->Incident->ParentIncident->find('list');
            $sims = $this->Incident->Sim->find('list');
            $this->set(compact('incidentTypes', 'severities', 'incidentStatuses', 'technicians', 'users', 'parentIncidents', 'sims'));
        }
    }

    function edit($id = null) {
        if (!empty($this->data)) {
            if ($this->Incident->save($this->data) == false)
                $this->Session->setFlash('Error saving Incident.');
            $this->redirect(array('action' => 'index'));
        } else {
            if ($id == null)
                die("No ID received");
            $this->data = $this->Incident->read(null, $id);
            $parents[0] = "[ Top ]";
            $incidents = $this->Incident->generateTreeList(null, null, null, '&nbsp;&nbsp;&nbsp;');
            if ($incidents)
                foreach ($incidents as $key => $value)
                    $parents[$key] = $value;
            $this->set(compact('parents'));
        }
    }

    function delete($id = null) {
        if ($id == null)
            die("No ID received");
        $this->Incident->id = $id;
        if ($this->Incident->removeFromTree($id, true) == false)
            $this->Session->setFlash('The Incident could not be deleted.');
        $this->Session->setFlash('Incident has been deleted.');
        $this->redirect(array('action' => 'index'));
    }

}

?>