<?php

App::uses('AppController', 'Controller');

/**
 * ContactsUpdates Controller
 *
 * @property ContactsUpdate $ContactsUpdate
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class ContactsUpdatesController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session');

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->ContactsUpdate->recursive = 0;
        $this->set('contactsUpdates', $this->Paginator->paginate());
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->ContactsUpdate->exists($id)) {
            throw new NotFoundException(__('Invalid contacts update'));
        }
        $options = array('conditions' => array('ContactsUpdate.' . $this->ContactsUpdate->primaryKey => $id));
        $this->set('contactsUpdate', $this->ContactsUpdate->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if ($this->request->is('post')) {
            $this->ContactsUpdate->create();
            if ($this->ContactsUpdate->save($this->request->data)) {
                $this->Session->setFlash(__('The contacts update has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The contacts update could not be saved. Please, try again.'));
            }
        }
        $contacts = $this->ContactsUpdate->Contact->find('list');
        $users = $this->ContactsUpdate->User->find('list');
        $this->set(compact('contacts', 'users'));
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->ContactsUpdate->exists($id)) {
            throw new NotFoundException(__('Invalid contacts update'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->ContactsUpdate->save($this->request->data)) {
                $this->Session->setFlash(__('The contacts update has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The contacts update could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('ContactsUpdate.' . $this->ContactsUpdate->primaryKey => $id));
            $this->request->data = $this->ContactsUpdate->find('first', $options);
        }
        $contacts = $this->ContactsUpdate->Contact->find('list');
        $users = $this->ContactsUpdate->User->find('list');
        $this->set(compact('contacts', 'users'));
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->ContactsUpdate->id = $id;
        if (!$this->ContactsUpdate->exists()) {
            throw new NotFoundException(__('Invalid contacts update'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->ContactsUpdate->delete()) {
            $this->Session->setFlash(__('The contacts update has been deleted.'));
        } else {
            $this->Session->setFlash(__('The contacts update could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

}
