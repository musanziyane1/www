<?php

App::uses('AppController', 'Controller');

/**
 * SimsUpdates Controller
 *
 * @property SimsUpdate $SimsUpdate
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class SimsUpdatesController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session');

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->SimsUpdate->recursive = 0;
        $this->set('simsUpdates', $this->Paginator->paginate());
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->SimsUpdate->exists($id)) {
            throw new NotFoundException(__('Invalid sims update'));
        }
        $options = array('conditions' => array('SimsUpdate.' . $this->SimsUpdate->primaryKey => $id));
        $this->set('simsUpdate', $this->SimsUpdate->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if ($this->request->is('post')) {
            $this->SimsUpdate->create();
            if ($this->SimsUpdate->save($this->request->data)) {
                $this->Session->setFlash(__('The sims update has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The sims update could not be saved. Please, try again.'));
            }
        }
        $apns = $this->SimsUpdate->Apn->find('list');
        $sims = $this->SimsUpdate->Sim->find('list');
        $this->set(compact('apns', 'sims'));
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->SimsUpdate->exists($id)) {
            throw new NotFoundException(__('Invalid sims update'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->SimsUpdate->save($this->request->data)) {
                $this->Session->setFlash(__('The sims update has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The sims update could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('SimsUpdate.' . $this->SimsUpdate->primaryKey => $id));
            $this->request->data = $this->SimsUpdate->find('first', $options);
        }
        $apns = $this->SimsUpdate->Apn->find('list');
        $sims = $this->SimsUpdate->Sim->find('list');
        $this->set(compact('apns', 'sims'));
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->SimsUpdate->id = $id;
        if (!$this->SimsUpdate->exists()) {
            throw new NotFoundException(__('Invalid sims update'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->SimsUpdate->delete()) {
            $this->Session->setFlash(__('The sims update has been deleted.'));
        } else {
            $this->Session->setFlash(__('The sims update could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

}
