<?php

App::uses('AppController', 'Controller');

/**
 * Severities Controller
 *
 * @property Severity $Severity
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class SeveritiesController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session');

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->Severity->recursive = 0;
        $this->set('severities', $this->Paginator->paginate());
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->Severity->exists($id)) {
            throw new NotFoundException(__('Invalid severity'));
        }
        $options = array('conditions' => array('Severity.' . $this->Severity->primaryKey => $id));
        $this->set('severity', $this->Severity->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if ($this->request->is('post')) {
            $this->Severity->create();
            if ($this->Severity->save($this->request->data)) {
                $this->Session->setFlash(__('The severity has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The severity could not be saved. Please, try again.'));
            }
        }
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->Severity->exists($id)) {
            throw new NotFoundException(__('Invalid severity'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->Severity->save($this->request->data)) {
                $this->Session->setFlash(__('The severity has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The severity could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('Severity.' . $this->Severity->primaryKey => $id));
            $this->request->data = $this->Severity->find('first', $options);
        }
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->Severity->id = $id;
        if (!$this->Severity->exists()) {
            throw new NotFoundException(__('Invalid severity'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->Severity->delete()) {
            $this->Session->setFlash(__('The severity has been deleted.'));
        } else {
            $this->Session->setFlash(__('The severity could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

}
