<?php

App::uses('AppController', 'Controller');

/**
 * CustomersUpdates Controller
 *
 * @property CustomersUpdate $CustomersUpdate
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class CustomersUpdatesController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session');

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->CustomersUpdate->recursive = 0;
        $this->set('customersUpdates', $this->Paginator->paginate());
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->CustomersUpdate->exists($id)) {
            throw new NotFoundException(__('Invalid customers update'));
        }
        $options = array('conditions' => array('CustomersUpdate.' . $this->CustomersUpdate->primaryKey => $id));
        $this->set('customersUpdate', $this->CustomersUpdate->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if ($this->request->is('post')) {
            $this->CustomersUpdate->create();
            if ($this->CustomersUpdate->save($this->request->data)) {
                $this->Session->setFlash(__('The customers update has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The customers update could not be saved. Please, try again.'));
            }
        }
        $customers = $this->CustomersUpdate->Customer->find('list');
        $users = $this->CustomersUpdate->User->find('list');
        $this->set(compact('customers', 'users'));
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->CustomersUpdate->exists($id)) {
            throw new NotFoundException(__('Invalid customers update'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->CustomersUpdate->save($this->request->data)) {
                $this->Session->setFlash(__('The customers update has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The customers update could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('CustomersUpdate.' . $this->CustomersUpdate->primaryKey => $id));
            $this->request->data = $this->CustomersUpdate->find('first', $options);
        }
        $customers = $this->CustomersUpdate->Customer->find('list');
        $users = $this->CustomersUpdate->User->find('list');
        $this->set(compact('customers', 'users'));
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->CustomersUpdate->id = $id;
        if (!$this->CustomersUpdate->exists()) {
            throw new NotFoundException(__('Invalid customers update'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->CustomersUpdate->delete()) {
            $this->Session->setFlash(__('The customers update has been deleted.'));
        } else {
            $this->Session->setFlash(__('The customers update could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

}
