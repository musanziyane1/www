<?php

App::uses('AppController', 'Controller');

/**
 * IncidentTypes Controller
 *
 * @property IncidentType $IncidentType
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class IncidentTypesController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session');

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->IncidentType->recursive = 0;
        $this->set('incidentTypes', $this->Paginator->paginate());
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->IncidentType->exists($id)) {
            throw new NotFoundException(__('Invalid incident type'));
        }
        $options = array('conditions' => array('IncidentType.' . $this->IncidentType->primaryKey => $id));
        $this->set('incidentType', $this->IncidentType->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if ($this->request->is('post')) {
            $this->IncidentType->create();
            if ($this->IncidentType->save($this->request->data)) {
                $this->Session->setFlash(__('The incident type has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The incident type could not be saved. Please, try again.'));
            }
        }
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->IncidentType->exists($id)) {
            throw new NotFoundException(__('Invalid incident type'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->IncidentType->save($this->request->data)) {
                $this->Session->setFlash(__('The incident type has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The incident type could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('IncidentType.' . $this->IncidentType->primaryKey => $id));
            $this->request->data = $this->IncidentType->find('first', $options);
        }
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->IncidentType->id = $id;
        if (!$this->IncidentType->exists()) {
            throw new NotFoundException(__('Invalid incident type'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->IncidentType->delete()) {
            $this->Session->setFlash(__('The incident type has been deleted.'));
        } else {
            $this->Session->setFlash(__('The incident type could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

}
