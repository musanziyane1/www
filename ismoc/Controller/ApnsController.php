<?php

App::uses('AppController', 'Controller');

/**
 * Apns Controller
 *
 * @property Apn $Apn
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class ApnsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session');

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->Apn->recursive = 0;
        $this->set('apns', $this->Paginator->paginate());
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->Apn->exists($id)) {
            throw new NotFoundException(__('Invalid apn'));
        }
        $options = array('conditions' => array('Apn.' . $this->Apn->primaryKey => $id));
        $this->set('apn', $this->Apn->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if ($this->request->is('post')) {
            $this->Apn->create();
            if ($this->Apn->save($this->request->data)) {
                $this->Session->setFlash(__('The apn has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The apn could not be saved. Please, try again.'));
            }
        }
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->Apn->exists($id)) {
            throw new NotFoundException(__('Invalid apn'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->Apn->save($this->request->data)) {
                $this->Session->setFlash(__('The apn has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The apn could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('Apn.' . $this->Apn->primaryKey => $id));
            $this->request->data = $this->Apn->find('first', $options);
        }
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->Apn->id = $id;
        if (!$this->Apn->exists()) {
            throw new NotFoundException(__('Invalid apn'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->Apn->delete()) {
            $this->Session->setFlash(__('The apn has been deleted.'));
        } else {
            $this->Session->setFlash(__('The apn could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

    public function apns_summary() {
        $this->Apn->recursive = 0;
        $this->set('apns', $this->Paginator->paginate());
    }

}
