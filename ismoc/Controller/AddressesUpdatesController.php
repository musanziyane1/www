<?php

App::uses('AppController', 'Controller');

/**
 * AddressesUpdates Controller
 *
 * @property AddressesUpdate $AddressesUpdate
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class AddressesUpdatesController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session');

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->AddressesUpdate->recursive = 0;
        $this->set('addressesUpdates', $this->Paginator->paginate());
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->AddressesUpdate->exists($id)) {
            throw new NotFoundException(__('Invalid addresses update'));
        }
        $options = array('conditions' => array('AddressesUpdate.' . $this->AddressesUpdate->primaryKey => $id));
        $this->set('addressesUpdate', $this->AddressesUpdate->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if ($this->request->is('post')) {
            $this->AddressesUpdate->create();
            if ($this->AddressesUpdate->save($this->request->data)) {
                $this->Session->setFlash(__('The addresses update has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The addresses update could not be saved. Please, try again.'));
            }
        }
        $addresses = $this->AddressesUpdate->Address->find('list');
        $users = $this->AddressesUpdate->User->find('list');
        $this->set(compact('addresses', 'users'));
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->AddressesUpdate->exists($id)) {
            throw new NotFoundException(__('Invalid addresses update'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->AddressesUpdate->save($this->request->data)) {
                $this->Session->setFlash(__('The addresses update has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The addresses update could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('AddressesUpdate.' . $this->AddressesUpdate->primaryKey => $id));
            $this->request->data = $this->AddressesUpdate->find('first', $options);
        }
        $addresses = $this->AddressesUpdate->Address->find('list');
        $users = $this->AddressesUpdate->User->find('list');
        $this->set(compact('addresses', 'users'));
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->AddressesUpdate->id = $id;
        if (!$this->AddressesUpdate->exists()) {
            throw new NotFoundException(__('Invalid addresses update'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->AddressesUpdate->delete()) {
            $this->Session->setFlash(__('The addresses update has been deleted.'));
        } else {
            $this->Session->setFlash(__('The addresses update could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

}
