<div class="contacts view">
    <h2><?php echo __('Contact'); ?></h2>
    <dl>
        <dt><?php echo __('Id'); ?></dt>
        <dd>
            <?php echo h($contact['Contact']['id']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('First Name'); ?></dt>
        <dd>
            <?php echo h($contact['Contact']['first_name']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Last Name'); ?></dt>
        <dd>
            <?php echo h($contact['Contact']['last_name']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Email'); ?></dt>
        <dd>
            <?php echo h($contact['Contact']['email']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Mobile'); ?></dt>
        <dd>
            <?php echo h($contact['Contact']['mobile']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Tel'); ?></dt>
        <dd>
            <?php echo h($contact['Contact']['tel']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Fax'); ?></dt>
        <dd>
            <?php echo h($contact['Contact']['fax']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Role'); ?></dt>
        <dd>
            <?php echo h($contact['Contact']['role']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Customer'); ?></dt>
        <dd>
            <?php echo $this->Html->link($contact['Customer']['id'], array('controller' => 'customers', 'action' => 'view', $contact['Customer']['id'])); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Created'); ?></dt>
        <dd>
            <?php echo h($contact['Contact']['created']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Modified'); ?></dt>
        <dd>
            <?php echo h($contact['Contact']['modified']); ?>
            &nbsp;
        </dd>
    </dl>
</div>
<div class="actions">
    <h3><?php echo __('Actions'); ?></h3>
    <ul>
        <li><?php echo $this->Html->link(__('Edit Contact'), array('action' => 'edit', $contact['Contact']['id'])); ?> </li>
        <li><?php echo $this->Form->postLink(__('Delete Contact'), array('action' => 'delete', $contact['Contact']['id']), array(), __('Are you sure you want to delete # %s?', $contact['Contact']['id'])); ?> </li>
        <li><?php echo $this->Html->link(__('List Contacts'), array('action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('New Contact'), array('action' => 'add')); ?> </li>
        <li><?php echo $this->Html->link(__('List Customers'), array('controller' => 'customers', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('New Customer'), array('controller' => 'customers', 'action' => 'add')); ?> </li>
    </ul>
</div>
