<div class="apns view">
    <h2><?php echo __('Apn'); ?></h2>
    <dl>
        <dt><?php echo __('Id'); ?></dt>
        <dd>
            <?php echo h($apn['Apn']['id']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Name'); ?></dt>
        <dd>
            <?php echo h($apn['Apn']['name']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Created'); ?></dt>
        <dd>
            <?php echo h($apn['Apn']['created']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Modified'); ?></dt>
        <dd>
            <?php echo h($apn['Apn']['modified']); ?>
            &nbsp;
        </dd>
    </dl>
</div>
<div class="actions">
    <h3><?php echo __('Actions'); ?></h3>
    <ul>
        <li><?php echo $this->Html->link(__('Edit Apn'), array('action' => 'edit', $apn['Apn']['id'])); ?> </li>
        <li><?php echo $this->Form->postLink(__('Delete Apn'), array('action' => 'delete', $apn['Apn']['id']), array(), __('Are you sure you want to delete # %s?', $apn['Apn']['id'])); ?> </li>
        <li><?php echo $this->Html->link(__('List Apns'), array('action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('New Apn'), array('action' => 'add')); ?> </li>
        <li><?php echo $this->Html->link(__('List Sims'), array('controller' => 'sims', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('New Sim'), array('controller' => 'sims', 'action' => 'add')); ?> </li>
    </ul>
</div>
<div class="related">
    <h3><?php echo __('Related Sims'); ?></h3>
    <?php if (!empty($apn['Sim'])): ?>
        <table cellpadding = "0" cellspacing = "0">
            <tr>
                <th><?php echo __('Id'); ?></th>
                <th><?php echo __('Status'); ?></th>
                <th><?php echo __('Name'); ?></th>
                <th><?php echo __('Created'); ?></th>
                <th><?php echo __('Modified'); ?></th>
                <th><?php echo __('Msisdn'); ?></th>
                <th><?php echo __('Iccid'); ?></th>
                <th><?php echo __('Mtd Usage'); ?></th>
                <th><?php echo __('Imsi'); ?></th>
                <th><?php echo __('Device Id'); ?></th>
                <th><?php echo __('Apn Id'); ?></th>
                <th><?php echo __('Online'); ?></th>
                <th class="actions"><?php echo __('Actions'); ?></th>
            </tr>
            <?php foreach ($apn['Sim'] as $sim): ?>
                <tr>
                    <td><?php echo $sim['id']; ?></td>
                    <td><?php echo $sim['status']; ?></td>
                    <td><?php echo $sim['name']; ?></td>
                    <td><?php echo $sim['created']; ?></td>
                    <td><?php echo $sim['modified']; ?></td>
                    <td><?php echo $sim['msisdn']; ?></td>
                    <td><?php echo $sim['iccid']; ?></td>
                    <td><?php echo $sim['mtd_usage']; ?></td>
                    <td><?php echo $sim['imsi']; ?></td>
                    <td><?php echo $sim['device_id']; ?></td>
                    <td><?php echo $sim['apn_id']; ?></td>
                    <td><?php echo $sim['online']; ?></td>
                    <td class="actions">
                        <?php echo $this->Html->link(__('View'), array('controller' => 'sims', 'action' => 'view', $sim['id'])); ?>
                        <?php echo $this->Html->link(__('Edit'), array('controller' => 'sims', 'action' => 'edit', $sim['id'])); ?>
                        <?php echo $this->Form->postLink(__('Delete'), array('controller' => 'sims', 'action' => 'delete', $sim['id']), array(), __('Are you sure you want to delete # %s?', $sim['id'])); ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>

    <div class="actions">
        <ul>
            <li><?php echo $this->Html->link(__('New Sim'), array('controller' => 'sims', 'action' => 'add')); ?> </li>
        </ul>
    </div>
</div>
