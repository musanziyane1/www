<div class="apns form">
    <?php echo $this->Form->create('Apn'); ?>
    <fieldset>
        <legend><?php echo __('Edit Apn'); ?></legend>
        <?php
        echo $this->Form->input('id');
        echo $this->Form->input('name');
        ?>
    </fieldset>
    <?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
    <h3><?php echo __('Actions'); ?></h3>
    <ul>

        <li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Apn.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('Apn.id'))); ?></li>
        <li><?php echo $this->Html->link(__('List Apns'), array('action' => 'index')); ?></li>
        <li><?php echo $this->Html->link(__('List Sims'), array('controller' => 'sims', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('New Sim'), array('controller' => 'sims', 'action' => 'add')); ?> </li>
    </ul>
</div>
