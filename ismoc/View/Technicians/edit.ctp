<div class="technicians form">
    <?php echo $this->Form->create('Technician'); ?>
    <fieldset>
        <legend><?php echo __('Edit Technician'); ?></legend>
        <?php
        echo $this->Form->input('id');
        echo $this->Form->input('first_name');
        echo $this->Form->input('last_name');
        echo $this->Form->input('id_number');
        echo $this->Form->input('email');
        echo $this->Form->input('mobile');
        echo $this->Form->input('tel');
        echo $this->Form->input('username');
        echo $this->Form->input('password');
        ?>
    </fieldset>
    <?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
    <h3><?php echo __('Actions'); ?></h3>
    <ul>

        <li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Technician.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('Technician.id'))); ?></li>
        <li><?php echo $this->Html->link(__('List Technicians'), array('action' => 'index')); ?></li>
        <li><?php echo $this->Html->link(__('List Incidents'), array('controller' => 'incidents', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('New Incident'), array('controller' => 'incidents', 'action' => 'add')); ?> </li>
        <li><?php echo $this->Html->link(__('List Incidents Updates'), array('controller' => 'incidents_updates', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('New Incidents Update'), array('controller' => 'incidents_updates', 'action' => 'add')); ?> </li>
    </ul>
</div>
