<?php echo $this->Html->addCrumb("Technicians Overview", "/Technicians/overview"); ?>
<?php echo $this->Html->addCrumb("View"); ?>
<div class="row">
        <div class="col-lg-6">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h4>Technician Details</h4>

                    <br /> 

                    <table class="table">

                        <tr class="active">
                            <td><b><?php echo __('First Name'); ?></b></td>
                            <td><?php echo h($technician['Technician']['first_name']); ?>&nbsp;</td>
                        </tr>

                        <tr class="active"> 
                            <td><b><?php echo __('Last Name'); ?></b></td>
                            <td><?php echo h($technician['Technician']['last_name']); ?>&nbsp;</td>

                        </tr>

                        <tr class="active">
                            <td><b><?php echo __('Id Number'); ?></b></td>
                            <td><?php echo h($technician['Technician']['id_number']); ?></td>		
                        </tr>	

                        <tr class="active">
                            <td><b><?php echo __('Email'); ?></b></td>
                            <td><?php echo h($technician['Technician']['email']); ?></td>
                        </tr>

                        <tr class="active">
                            <td><b><?php echo __('Mobile'); ?></b></td>
                            <td><?php echo h($technician['Technician']['mobile']); ?></td>
                        </tr>

                        <tr class="active">
                            <td><b><?php echo __('Tel'); ?></b></td>
                            <td><?php echo h($technician['Technician']['tel']); ?></td>
                        </tr>


                    </table>
                    <br>


                    <div class="related">
                        <h3><?php echo __('Related Incidents'); ?></h3>
                        <?php if (!empty($technician['Incident'])): ?>
                            <table cellpadding = "0" cellspacing = "0">
                                <tr>
                                    <th><?php echo __('Id'); ?></th>
                                    <th><?php echo __('Description'); ?></th>


                                </tr>
                                <?php foreach ($technician['Incident'] as $incident): ?>
                                    <tr>
                                        <td><?php echo $incident['id']; ?></td>
                                        <td><?php echo $incident['description']; ?></td>



                                        <?php echo $this->Html->link(__('$incident["description"]'), array('controller' => 'incidents', 'action' => 'view', $incident['id'])); ?>


                                    </tr>
                                <?php endforeach; ?>
                            </table>
                        <?php endif; ?>


                    </div>


                </div>

            </div>
        </div>


        <div class="row">
            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h4> Update Technician Details</h4>

                        <br />
                        <div class="technicians form">
                            <?php echo $this->Form->create('Technician'); ?>
                            <fieldset>
                                <legend><?php echo __('Edit Technician'); ?></legend>
                                <?php
                                echo $this->Form->input('id');
                                echo $this->Form->input('first_name', array('disabled' => true, 'class' => "form-control"));
                                echo $this->Form->input('last_name', array('disabled' => true, 'class' => "form-control"));
                                echo $this->Form->input('id_number', array('disabled' => true, 'class' => "form-control"));
                                echo $this->Form->input('email', array('class' => "form-control"));
                                echo $this->Form->input('mobile', array('class' => "form-control"));
                                echo $this->Form->input('tel', array('class' => "form-control"));
                                echo $this->Form->input('username', array('class' => "form-control"));
                                echo $this->Form->input('password', array('class' => "form-control"));
                                ?>
                            </fieldset><br />
                            <?php echo $this->Form->submit('Submit', array('class' => 'btn btn-primary')); ?>
                        </div>

                    </div>
                </div>
            </div>
        </div> 
