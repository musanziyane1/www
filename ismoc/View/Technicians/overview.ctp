<?php echo $this->Html->addCrumb("Technicians Overview"); ?>

<div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">

                        <h4>Technicians Overview</h4>

                        <div class='table-responsive'>
                            <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">

                                <thead>
                                    <tr>
                                        <th><?php echo $this->Paginator->sort('id'); ?></th>
                                        <th><?php echo $this->Paginator->sort('first_name'); ?></th>
                                        <th><?php echo $this->Paginator->sort('last_name'); ?></th>
                                        <th><?php echo $this->Paginator->sort('id_number'); ?></th>
                                        <th><?php echo $this->Paginator->sort('email'); ?></th>
                                        <th><?php echo $this->Paginator->sort('mobile'); ?></th>
                                        <th><?php echo $this->Paginator->sort('tel'); ?></th>
                                        <td></td>
                                    </tr>

                                    </head>
                                <thead>
                                    <tr>
                                        <td><b><?php echo 'ID'; ?></b></td>
                                        <td><b><?php echo 'First name'; ?></b></td>
                                        <td><b><?php echo 'Last name'; ?></b></td>
                                        <td><b><?php echo 'Id number'; ?></b></td>
                                        <td><b><?php echo 'Email'; ?></b></td>
                                        <td><b><?php echo 'Mobile'; ?></b></td>
                                        <td><b><?php echo 'Tel'; ?></b></td>
                                        <td><b><?php echo 'Action'; ?></b></td>

                                    </tr>
                                </thead>

                                <?php foreach ($technicians as $technician): ?>
                                    <tr>
                                        <td><?php echo h($technician['Technician']['id']); ?>&nbsp;</td>
                                        <td><?php echo h($technician['Technician']['first_name']); ?>&nbsp;</td>
                                        <td><?php echo h($technician['Technician']['last_name']); ?>&nbsp;</td>
                                        <td><?php echo h($technician['Technician']['id_number']); ?>&nbsp;</td>
                                        <td><?php echo h($technician['Technician']['email']); ?>&nbsp;</td>
                                        <td><?php echo h($technician['Technician']['mobile']); ?>&nbsp;</td>
                                        <td><?php echo h($technician['Technician']['tel']); ?>&nbsp;</td>
                                        <td><?php echo $this->Html->link(__('View'), array('action' => 'view', $technician['Technician']['id'])); ?></td>

                                    </tr>
                                <?php endforeach; ?>
                            </table>

                        </div>

                    </div>
                </div>
            </div>

        </div>