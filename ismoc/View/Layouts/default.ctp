<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $title_for_layout; ?>
	</title>
	
	<?php
		echo $this->Html->meta('icon');
		echo $this->Html->css('bootstrap.min');
		echo $this->Html->css('styles');
		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	
		$action = $this->request->params['action'];
		$controller = $this->request->params['controller'];
	?>
	  <script src="http://cdn.datatables.net/1.10.2/css/jquery.dataTables.css"></script>
<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="http://cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>

<script src="http://cdn.datatables.net/plug-ins/725b2a2115b/integration/bootstrap/3/dataTables.bootstrap.js"></script>

<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css"></script>
 <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">
<script src="http://cdn.datatables.net/plug-ins/725b2a2115b/integration/bootstrap/3/dataTables.bootstrap.js"></script>
  <script src="http://code.highcharts.com/highcharts.js"></script>
	<script type="text/javascript"
	      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB4V8IqCYklDt3vR5ujJb1d3fVBb31nkRY&sensor=false">
	    </script>
	    
	
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>	
<script src="http://code.jquery.com/jquery-1.10.2.min.js"></script>
<script src="http://cdn.datatables.net/1.10.0/js/jquery.dataTables.js"></script>
<script>
$(document).ready(function() {
    $('#data-table').dataTable();
} );
</script>
<script>
$(function() {
 $('.colors').hide();
        $('#model').change(function(){
           $('.colors').hide();
            $('#' + $(this).val()).show();
        });
    });
</script>

</head>
<body>
 <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div id="container">
        <div class="navbar-header">
          <img src="/img/mtn_logo.jpg" width=120px />
          <a class='maverick-heading' href="#">MTN SMART Metering</a>
        </div>
		
		     <div class="navbar-collapse collapse">
			  <ul class="nav navbar-nav pull-right">
			   <li class=""><a href="#">Help</a></li> 

		  <?php
                if ($this->Session->check('Auth.User')) {
                echo '<li class=""><a href="/users/logout">Logout</a></li>';

                } else {
                echo "";
                }
                ?> </ul><br /><br /><br />
         <div class="col-sm-2 col-md-3 pull-right">
		 
            <form class="navbar-form" role="search">
			
                <div class="input-group">

                    <input type="text" class="form-control" placeholder="Search" name="q">
                    <div class="input-group-btn">
						
                        <a href="#" class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></a>
                    </div>
                </div>
            </form>
        </div>   
		 <div class="col-lg-3">
		 <form class="navbar-form" role="search">
<Select id="model" class="form-control">
   <option value="sim">Sim</option>
   <option value="customer">Customer</option>
   <option value="incident">Incident</option>
    <option value="technician">Technician</option>
    
</Select>
</form>
</div>		
		 <div class="col-lg-2 pull-right">
		 <form class="navbar-form" role="search">
         
		  <select id="sim" class="colors form-control" style="display:none">
    <option>Imsi</option>
    <option>msidn</option>
    <option>Status</option>
</select>

<select id="customer" class="colors form-control" style="display:none">
    <option>First name</option>
    <option>Last name</option>
	<option>Id number</option>   
    <option>Email</option>
    <option>Phone Number</option>
</select>

<select id="incident" class="colors form-control" style="display:none">
    <option>Type</option>
    <option>Status</option>
    <option>Technician</option>
</select>

<select id="technician" class="colors form-control" style="display:none">
    <option>First name</option>
    <option>Last name</option>
	<option>Id number</option>    
    <option>Employee id</option>    
    <option>Email</option>    
    
</select>
</form>
</div>		
		 

        </div><!--/.navbar-collapse -->
      
       </div>
    </div>

	<div id='container' class="push-down">
		<div class="row">
			<div class="col-md-12">
		         <ol class="breadcrumb">
				<?php echo $this->Html->getCrumbs(' > ', array( 
    'url' => array('controller' => 'sims', 'action' => 'dashboard', 'Dashboard'),
    'escape' => false
)); ?> 
					
				</ol>
				<?php echo $this->Session->flash('good');?>
			</div>
		</div>
		<script src="http://cdn.datatables.net/plug-ins/725b2a2115b/integration/bootstrap/3/dataTables.bootstrap.js"></script>
			
            <div class="container">
            <?php echo $this->fetch('content'); ?>
			</div>
	</div>
	<?php //echo $this->element('sql_dump'); ?>
    
    <script>
    $(document).ready(function() {
        $('#example').dataTable();
    });

</script>

<script>
    $(document).ready(function() {
        var table = $('#example').DataTable();

        $("#example thead th").each(function(i) {
            var select = $('<select><option value=""></option></select>')
                    .appendTo($(this).empty())
                    .on('change', function() {
                        var val = $(this).val();

                        table.column(i)
                                .search(val ? '^' + $(this).val() + '$' : val, true, false)
                                .draw();
                    });

            table.column(i).data().unique().sort().each(function(d, j) {
                select.append('<option value="' + d + '">' + d + '</option>')
            });
        });
    });
</script>

</body>
</html>

