<div class="well">
    <!--login modal--> 
    <div id="loginModal" class="modal show" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <?php echo $this->Session->flash('auth'); ?>

                <?php echo $this->Form->create('User'); ?>

                <div class="modal-header">
                    <h3 class="text-center">Please sign in</h3>
                </div>
                <div class="modal-body">

                    <div class="form-group">
                        <?php echo $this->Form->input('username', array('type' => 'username', 'class' => 'form-control')); ?>
                    </div>
                    <div class="form-group">
                        <?php echo $this->Form->input('password', array('type' => 'password', 'class' => 'form-control')); ?>
                    </div>
                    <!---	<div class="form-group">
                    <?php echo $this->Form->input('rememberMe', array('type' => 'checkbox', 'label' => 'Remember me')); ?>
                            </div>	  -->	
                    <button class="btn btn-success btn-lg btn-block">Sign In</button>
                    <!--<?php
                    echo $this->Html->link("Add A New User", array('action' => 'add'));
                    ?>-->
                </div>
            </div>
        </div>
    </div>

</div>
