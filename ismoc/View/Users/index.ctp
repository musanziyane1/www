<?php
echo $this->Html->addCrumb("Connectivity", "/incidents/dashboard");
echo $this->Html->addcrumb('Reports');
?>
<div class="col-md-3">	
    <div class="well">
        <ul class="nav nav-pills nav-stacked">
            <li><a href="/devices/dashboard">Connectivity Map</a></li>
            <li><a href="/incidents/index">Incident Management</a></li>
            <li><a href="/sims/sim_status">Sim Status</a></li>
            <li><a href="/customers/index">Customers Data</a></li>
            <li><a href="/assets/index">Assets Data</a></li>
            <li><a href="/devices/index">Devices Data</a></li>   
            <li><a href="/sims/sim_data">Sims Data</a></li>
            <li><a href="/sims/on_demand_sim_signal_strength">On demand Sim Signal Strength</a></li>
            <li><a href="/sims/on_demand_sim_connection_speed_test">Sim Connection Speed Test</a></li>
            <li><a href="/sims/on_demand_signal_quality_test">Signal Quality Test</a></li>
            <li><a href="/sims/on_demand_network_performance_and_bandwidth">Network performance and bandwidth usage</a></li>
            <li class="active"><a href="/users/index">Reports</a></li>
        </ul>
    </div>
</div> 

<div class="col-md-9">
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">

                <div class="col-xs-12">	

                    <h2>Reports</h2>
                    <!-- List group -->
                    <div class="list-group">

                        <a href="/incidents/incident_summary" class="list-group-item">Incident Summary</span>
                        </a>
                        <a href="/assets/assets_summary" class="list-group-item">Assets Summary</span>
                        </a>     
                        <a href="/devices/devices_summary" class="list-group-item">Devices Summary</span>
                        </a>  
                        </a>
                        <a href="/sims/sims_summary" class="list-group-item">Sims Summary</span>
                        </a>  
                        <a href="/customers/customers_summary" class="list-group-item">Customers Summary</a>
                        <a href="/apns/apns_summary" class="list-group-item">Apns Summary</a>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>