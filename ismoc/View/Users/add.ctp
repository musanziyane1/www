
<div class="well">
    <!-- app/View/Users/add.ctp -->
    <!--login modal-->
    <div id="loginModal" class="modal show" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <?php echo $this->Form->create('User'); ?>

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 class="text-center">Add new user</h3>
                </div>
                <div class="modal-body">
                    <?php echo $this->Form->input('username'); ?>
                    <?php echo $this->Form->input('email'); ?>
                    <?php echo $this->Form->input('password'); ?>
                    <?php echo $this->Form->input('password_confirm', array('label' => 'Confirm Password *', 'maxLength' => 255, 'title' => 'Confirm password', 'type' => 'password')); ?>
                    <?php
                    echo $this->Form->input('role', array(
                        'options' => array('developer' => 'Developer', 'admin' => 'Admin', 'technician' => 'Technician', 'customer' => 'Customer')
                    ));
                    ?>

                    <button class="btn btn-primary btn-lg">Submit</button>

                </div>
                <?php
                if ($this->Session->check('Auth.User')) {
                    echo $this->Html->link("Return to Dashboard", array('controller' => 'incidents', 'action' => 'dashboard'));
                    echo "<br>";
                    echo $this->Html->link("Logout", array('action' => 'logout'));
                } else {
                    echo $this->Html->link("Return to Login Screen", array('action' => 'login'));
                }
                ?>
            </div>
        </div>
    </div>
</div>