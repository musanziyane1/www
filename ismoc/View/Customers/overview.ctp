<?php echo $this->Html->addCrumb(" Customers Overview"); ?>


    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">

                    <h4>Customers Overview</h4>


                    <div class='table-responsive'>
                        <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th><?php echo $this->Paginator->sort('id'); ?></th>
                                    <th><?php echo $this->Paginator->sort('first_name'); ?></th>
                                    <th><?php echo $this->Paginator->sort('last_name'); ?></th>
                                    <th><?php echo $this->Paginator->sort('id_number'); ?></th>
                                    <th><?php echo $this->Paginator->sort('email'); ?></th>
                                    <td></td>

                                </tr>
                            </thead>
                            <thead>
                                <tr>
                                    <td><?php echo '<b>ID</b>'; ?></td>
                                    <td><?php echo '<b>First name</b>'; ?></td>
                                    <td><?php echo '<b>Last name</b>'; ?></td>
                                    <td><?php echo '<b>Id number</b>'; ?></td>
                                    <td><?php echo '<b>Email</b>'; ?></td>
                                    <td><?php echo '<b>Action</b>'; ?></td>

                                </tr>
                            </thead>
                            <?php foreach ($customers as $customer): ?>
                                <tr>
                                    <td><?php echo h($customer['Customer']['id']); ?>&nbsp;</td>
                                    <td><?php echo h($customer['Customer']['first_name']); ?>&nbsp;</td>
                                    <td><?php echo h($customer['Customer']['last_name']); ?>&nbsp;</td>
                                    <td><?php echo h($customer['Customer']['id_number']); ?>&nbsp;</td>
                                    <td><?php echo h($customer['Customer']['email']); ?>&nbsp;</td>
                                    <td><?php echo $this->Html->link(__('View'), array('action' => 'view', $customer['Customer']['id'])); ?></td>

                                </tr>
                            <?php endforeach; ?>
                        </table>


                    </div>
                </div>


            </div>
        </div>
    </div>
