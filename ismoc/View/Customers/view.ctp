<?php echo $this->Html->addCrumb("Customers Overview", "/customers/overview"); ?>
<?php echo $this->Html->addCrumb("View"); ?>
<div class="row">
        <div class="col-lg-6">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h4>Customer Details</h4>

                    <table class="table">

                        <tr class="active">

                            <td><b><?php echo __('Customer Number'); ?></b></td>

                            <td><?php echo h($customer['Customer']['id']); ?></td>
                        </tr>
                        <tr class="active">
                            <td><b><?php echo __('First Name'); ?></b></td>
                            <td><?php echo h($customer['Customer']['first_name']); ?></td>
                        </tr>

                        <tr class="active"> 
                            <td><b><?php echo __('Last Name'); ?></b></td>
                            <td><?php echo h($customer['Customer']['last_name']); ?></td>
                        </tr>		

                        <tr class="active">
                            <td><b><?php echo __('Id Number'); ?></b></td>
                            <td><?php echo h($customer['Customer']['id_number']); ?></td>		
                        </tr>

                        <tr class="active">
                            <td><b><?php echo __('Mobile'); ?></b></td>
                            <td><?php echo h($customer['Customer']['mobile']); ?></td>
                        </tr>

                        <tr class="active">
                            <td><b><?php echo __('Tel'); ?></b></td>
                            <td><?php echo h($customer['Customer']['tel']); ?></td>
                        </tr>

                        <tr class="active">
                            <td><b><?php echo __('Fax'); ?></b></td>
                            <td><?php echo h($customer['Customer']['fax']); ?></td>
                        </tr>

                        <?php if (!empty($customer['Sim'])): ?>
                            <tr class="active">

                                <td><b><?php echo __('Sims'); ?></b></td>
                                <?php foreach ($customer['Sim'] as $sim): ?>

                                    <td><?php echo $this->Html->link(__($sim['id']), array('controller' => 'sims', 'action' => 'view', $sim['id'])); ?></td>
                                <?php endforeach; ?>

                            </tr>

                            <tr class="active">
                                <?php foreach ($customer['Contact'] as $contact): ?>
                                    <td><b><?php echo __('Other Contacts'); ?></b></td>


                                    <td><?php echo $this->Html->link(__($contact['id']), array('controller' => 'sims', 'action' => 'view', $contact['id'])); ?></td>
                                <?php endforeach; ?>

                            </tr>
                        <?php endif; ?>


                    </table>

                    <br/>

                    <h4>Main Address</h4>

                    <table class="table">

                        <tr class="active">

                            <td><b><?php echo __('Address Line 1'); ?></b></td>

                            <td><?php echo h($customer['Customer']['address_line_1']); ?></td>
                        </tr>
                        <tr class="active">

                            <td><b><?php echo __('Address Line 2'); ?></b></td>

                            <td><?php echo h($customer['Customer']['address_line_2']); ?></td>
                        </tr>

                        <tr class="active">

                            <td><b><?php echo __('Suburb'); ?></b></td>

                            <td><?php echo h($customer['Customer']['suburb']); ?></td>
                        </tr>

                        <tr class="active">

                            <td><b><?php echo __('City'); ?></b></td>

                            <td><?php echo h($customer['Customer']['city']); ?></td>
                        </tr>

                        <tr class="active">

                            <td><b><?php echo __('Municipality'); ?></b></td>

                            <td><?php echo h($customer['Customer']['municipality']); ?></td>
                        </tr>

                        <tr class="active">

                            <td><b><?php echo __('Province'); ?></b></td>

                            <td><?php echo h($customer['Customer']['province']); ?></td>
                        </tr>

                        <tr class="active">

                            <td><b><?php echo __('Zip Code'); ?></b></td>

                            <td><?php echo h($customer['Customer']['zip_code']); ?></td>
                        </tr>

                        <tr class="active">
                            <?php foreach ($customer['Address'] as $address): ?>
                                <td><b><?php echo __('Other Contacts'); ?></b></td>


                                <td><?php echo $this->Html->link(__($address[' address_line_1']), array('controller' => 'sims', 'action' => 'view', $address['id'])); ?></td>
                            <?php endforeach; ?>

                        </tr>

                    </table>
                    <br/>

                </div>
            </div>
        </div>	


        <div class="row">
            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h4> Update Customer Details</h4>
                        <div class="customers form">
                            <?php echo $this->Form->create('Customer'); ?>
                            <fieldset>
                                <legend><?php echo __('Edit Customer'); ?></legend>
                                <?php
                                echo $this->Form->input('id');
                                echo $this->Form->input('first_name', array('disabled' => true, 'class' => "form-control"));
                                echo $this->Form->input('last_name', array('disabled' => true, 'class' => "form-control"));
                                echo $this->Form->input('id_number', array('disabled' => true, 'class' => "form-control"));
                                echo $this->Form->input('email', array('class' => "form-control"));
                                echo $this->Form->input('mobile', array('class' => "form-control"));
                                echo $this->Form->input('tel', array('class' => "form-control"));
                                echo $this->Form->input('fax', array('class' => "form-control"));
                                echo $this->Form->input('address_line_1', array('class' => "form-control"));
                                echo $this->Form->input('address_line_2', array('class' => "form-control"));
                                echo $this->Form->input('suburb', array('class' => "form-control"));
                                echo $this->Form->input('city', array('class' => "form-control"));
                                echo $this->Form->input('municipality', array('class' => "form-control"));
                                echo $this->Form->input('province', array('class' => "form-control"));
                                echo $this->Form->input('zip_code', array('class' => "form-control"));
                                ?>
                            </fieldset><br />
                            <?php echo $this->Form->submit('Submit', array('class' => 'btn btn-primary')); ?>
                        </div>
                    </div>
                </div>


            </div>


        </div>

    </div>
