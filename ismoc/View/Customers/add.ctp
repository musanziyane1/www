<div class="customers form">
    <?php echo $this->Form->create('Customer'); ?>
    <fieldset>
        <legend><?php echo __('Add Customer'); ?></legend>
        <?php
        echo $this->Form->input('first_name');
        echo $this->Form->input('last_name');
        echo $this->Form->input('id_number');
        echo $this->Form->input('email');
        echo $this->Form->input('mobile');
        echo $this->Form->input('tel');
        echo $this->Form->input('fax');
        echo $this->Form->input('address_line_1');
        echo $this->Form->input('address_line_2');
        echo $this->Form->input('suburb');
        echo $this->Form->input('city');
        echo $this->Form->input('municipality');
        echo $this->Form->input('province');
        echo $this->Form->input('zip_code');
        ?>
    </fieldset>
    <?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
    <h3><?php echo __('Actions'); ?></h3>
    <ul>

        <li><?php echo $this->Html->link(__('List Customers'), array('action' => 'index')); ?></li>
        <li><?php echo $this->Html->link(__('List Addresses'), array('controller' => 'addresses', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('New Address'), array('controller' => 'addresses', 'action' => 'add')); ?> </li>
        <li><?php echo $this->Html->link(__('List Contacts'), array('controller' => 'contacts', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('New Contact'), array('controller' => 'contacts', 'action' => 'add')); ?> </li>
        <li><?php echo $this->Html->link(__('List Sims'), array('controller' => 'sims', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('New Sim'), array('controller' => 'sims', 'action' => 'add')); ?> </li>
    </ul>
</div>
