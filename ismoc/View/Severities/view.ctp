<div class="severities view">
<h2><?php echo __('Severity'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($severity['Severity']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Description'); ?></dt>
		<dd>
			<?php echo h($severity['Severity']['description']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Severity'), array('action' => 'edit', $severity['Severity']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Severity'), array('action' => 'delete', $severity['Severity']['id']), array(), __('Are you sure you want to delete # %s?', $severity['Severity']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Severities'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Severity'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Incidents'), array('controller' => 'incidents', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Incident'), array('controller' => 'incidents', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Incidents'); ?></h3>
	<?php if (!empty($severity['Incident'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Description'); ?></th>
		<th><?php echo __('Device Id'); ?></th>
		<th><?php echo __('Severity Id'); ?></th>
		<th><?php echo __('Status Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Origin'); ?></th>
		<th><?php echo __('Date Logged'); ?></th>
		<th><?php echo __('Title'); ?></th>
		<th><?php echo __('Imei'); ?></th>
		<th><?php echo __('Serial'); ?></th>
		<th><?php echo __('Last Communicated'); ?></th>
		<th><?php echo __('Longitude'); ?></th>
		<th><?php echo __('Latitude'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($severity['Incident'] as $incident): ?>
		<tr>
			<td><?php echo $incident['id']; ?></td>
			<td><?php echo $incident['description']; ?></td>
			<td><?php echo $incident['device_id']; ?></td>
			<td><?php echo $incident['severity_id']; ?></td>
			<td><?php echo $incident['status_id']; ?></td>
			<td><?php echo $incident['user_id']; ?></td>
			<td><?php echo $incident['origin']; ?></td>
			<td><?php echo $incident['date_logged']; ?></td>
			<td><?php echo $incident['title']; ?></td>
			<td><?php echo $incident['imei']; ?></td>
			<td><?php echo $incident['serial']; ?></td>
			<td><?php echo $incident['last_communicated']; ?></td>
			<td><?php echo $incident['longitude']; ?></td>
			<td><?php echo $incident['latitude']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'incidents', 'action' => 'view', $incident['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'incidents', 'action' => 'edit', $incident['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'incidents', 'action' => 'delete', $incident['id']), array(), __('Are you sure you want to delete # %s?', $incident['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Incident'), array('controller' => 'incidents', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
