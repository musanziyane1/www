<div class="severities form">
<?php echo $this->Form->create('Severity'); ?>
	<fieldset>
		<legend><?php echo __('Edit Severity'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('description');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Severity.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('Severity.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Severities'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Incidents'), array('controller' => 'incidents', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Incident'), array('controller' => 'incidents', 'action' => 'add')); ?> </li>
	</ul>
</div>
