<?php
echo $this->Html->addCrumb(' Devices List ', '/devices/index');
echo $this->Html->addCrumb(" Edit Device: " . $device['Device']['name']);

?>
<div class="col-md-3">	
<div class="well">
			<ul class="nav nav-pills nav-stacked">
				<li class="active"><a href="/devices/single_ip_address_allocation">Single Allocation of Static IP Address</a></li>
				<li><a href="/devices/batch_ip_address_allocation">Batch Allocation of Static IP Address</a></li>
				<li><a href="/sims/vendor_sim_allocation">Vendor SIM Allocation</a></li>
				<li><a href="/devices/installation_report">Installation Report</a></li>
				
  			</ul>
</div>
</div>
<div class="col-md-9">
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">

                <div class="col-xs-12">
<div class="devices form">
<?php echo $this->Form->create('Device'); ?>
	<fieldset>
		<legend><?php echo __('Edit Device'); ?></legend>
	<div class="form-group">
		
		<?php echo $this->Form->input('pin', array('type' => 'hidden', 'class' => 'form-control')); ?>
		<?php echo $this->Form->input('latitude', array('type' => 'hidden', 'class' => 'form-control')); ?>
		<?php echo $this->Form->input('longitude', array('type' =>'hidden', 'class' => 'form-control')); ?>
		<?php echo $this->Form->input('serial_number', array('type' => 'hidden', 'class' => 'form-control')); ?>
		<?php echo $this->Form->input('registered', array('type'=> 'hidden', 'class' => 'form-control')); ?>
		<?php echo $this->Form->input('imei', array('class' => 'form-control')); ?>
		<?php echo $this->Form->input('name', array('class' => 'form-control')); ?>
		<?php echo $this->Form->input('asset_id', array('type'=>'hidden', 'class' => 'form-control')); ?>
		<?php echo $this->Form->input('ip_address', array('class' => 'form-control')); ?>
		

	</div>
	
	</fieldset>
<?php echo $this->Form->submit( 'Change ip address Device' , array('type' => 'submit', 'class' => 'btn btn-success '));?>	 
                        <?php echo $this->Form->end(); ?>
</div>
 </div>
            </div>
        </div>
    </div>
</div>	