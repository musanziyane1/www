<?php
	echo $this->Html->addCrumb(" Device Specific", "/Devices/execute_ota_firmware_upgrade");
	echo $this->Html->addCrumb(' Monitor OTA Firmware Upgrade');
?>
	<!-- Example row of columns -->
      
       <div class="col-lg-3">
		 <div class="well">
			<ul class="nav nav-pills nav-stacked">
				<li><a href="/devices/execute_ota_firmware_upgrade">Execute OTA Firmware Upgrade</a></li>
				<li class="active"><a href="/devices/monitor_ota_firmware_upgrade">Monitor OTA Firmware Upgrade</a></li>
  			</ul>
        </div>
	</div>	
	</div>
	
	<script LANGUAGE="JavaScript" type="text/javascript">
		function display(){		
				$(document).ready(function(){

					var progress = setInterval(function() {
					var $bar = $('.bar');

					if ($bar.width()==400) {
						clearInterval(progress);
						$('.progress').removeClass('active');
					} else {
						$bar.width($bar.width()+40);
					}
					$bar.text($bar.width()/4 + "%");
				}, 800);

				})
		};
		
	</script>
	
    <div class="col-lg-9">
	  <div class="panel panel-default">
		<div class="panel-body"> 
          <h3>Monitor OTA Firmware Upgrade</h3>
          <form role="form">
				  <div class="form-group">
				    <label for="exampleInputEmail1">IMSI Number</label>
				    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter IMSI">
				  </div>
				  
				  <a class="btn btn-success"  onClick="display()">Monitor IMSI firmware upgrade</a>
    <div class="form-group" style="margin-top: 10px;">
		<div class="container">
			<div class="progress progress-success active" style="width: 0%;"></div>
			<div class="bar" style="width: 0%;"></div>	
		</div>
     </div>
	
            <div class="form-group" style="margin-top: 10px;">
	          <div class="well">
	          12:00:01 - Unpacking firmware downloading<br>
	          12:00:05 - Installing components<br>
	          12:00:09 - Updating GSM configuration<br>
	          </div>
            </div>

            </div>
          </form>
		  </div>
	   </div>
       </div>
      <hr>
    </div> <!-- /container -->