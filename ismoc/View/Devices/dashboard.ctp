<div class="col-md-3">
    <div class="well">
	    <ul class="nav nav-pills nav-stacked">
            <li class="active"><a href="/devices/dashboard">Connectivity Map</a></li>
            <li><a href="/incidents/index">Incident Management</a></li>
            <li><a href="/sims/sim_status">Sim Status</a></li>
			<li><a href="/customers/index">Customers Data</a></li>
			<li><a href="/assets/index">Assets Data</a></li>
			<li><a href="/devices/index">Devices Data</a></li>
            <li><a href="/sims/sim_data">Sims Data</a></li>
			<li><a href="/sims/on_demand_sim_signal_strength">On demand Sim Signal Strength</a></li>
			<li><a href="/sims/on_demand_sim_connection_speed_test">Sim Connection Speed Test</a></li>
			<li><a href="/sims/on_demand_signal_quality_test">Signal Quality Test</a></li>
			<li><a href="/sims/on_demand_network_performance_and_bandwidth">Network performance and bandwidth usage</a></li>
            <li><a href="/users/index">Reports</a></li>
        </ul>
    </div><!-- well -->
</div><!-- col-md-2 -->	

<div class="col-md-9">
    <div class="panel panel-default">
        <div class="panel-body">

					
					<legend><?php echo __('Modem Connectivity Viewer'); ?></legend>
					
					<!-- map -->
                   <div id="map" style="height: 460px;"></div>
               
        </div><!-- panel-body -->
    </div><!-- default panel -->
</div><!-- col-md-10 -->


<script src="http://maps.google.com/maps/api/js?sensor=false" 
          type="text/javascript"></script>

  

  <script type="text/javascript">
    var locations = [
	<?php foreach ($devices as $device): ?>
	 
	 <?php 
	 
	 $latitude= h($device['Device']['latitude']); 
	 $longitude = h($device['Device']['longitude']);
	 $name = h($device['Device']['name']); 
	 $serial_number = h($device['Device']['serial_number']); 
	 $imei = h($device['Device']['imei']); 
	 $registered = h($device['Device']['registered']); 
	 ?>
	
     ['<?php echo $name; ?>', <?php echo $latitude; ?>, <?php echo $longitude; ?>, '<?php echo  $serial_number; ?>', '<?php echo  $imei; ?>', '<?php echo $registered; ?>'],
     
	 <?php endforeach; ?>
    ];

    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 6,
       center: new google.maps.LatLng(-29.446773, 24.076109),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();

    var marker, i;
	
    for (i = 0; i < locations.length; i++) {  
		
		
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map,
		
      });

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
         window.location = "/incidents/add/" + i;
        }
      })(marker, i));
    }
  </script>
</body>