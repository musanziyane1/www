<?php
echo $this->Html->addCrumb(' Devices List ', '/devices/index');
echo $this->Html->addCrumb(" Edit Device: " . $device['Device']['name']);

?>
<div class="col-md-3">
    <div class="well">
        <ul class="nav nav-pills nav-stacked">
            <li><a href="/devices/dashboard">Connectivity Map</a></li>
            <li><a href="/incidents/index">Incident Management</a></li>
            <li class="active"><a href="/assets/index">Asset List</a></li>
            <li><a href="/devices/device_summary">Modem List</a></li>
            <li><a href="/sims/sim_status">Sim List</a></li>
            <li><a href="/sims/sim_data">SIM Data</a></li>
            <li><a href="/sims/on_demand_sim_signal_strength">On demand SIM Signal Strength</a></li>
            <li><a href="/sims/on_demand_sim_connection_speed_test">SIM Connection Speed Test</a></li>
            <li><a href="/sims/on_demand_signal_quality_test">Signal Quality Test</a></li>
            <li><a href="/sims/on_demand_network_performance_and_bandwidth">Network performance and bandwidth usage</a></li>
            <li><a href="/users/index">Reports</a></li>
        </ul>
    </div><!-- well -->
</div><!-- col-md-2 -->
<div class="col-md-9">
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">

                <div class="col-xs-12">
<div class="devices form">
<?php echo $this->Form->create('Device'); ?>
	<fieldset>
		<legend><?php echo __('Edit Device'); ?></legend>
	<div class="form-group">
		
		<?php echo $this->Form->input('pin', array('type' => 'hidden', 'class' => 'form-control')); ?>
		<?php echo $this->Form->input('latitude', array('type' => 'hidden', 'class' => 'form-control')); ?>
		<?php echo $this->Form->input('longitude', array('type' =>'hidden', 'class' => 'form-control')); ?>
		<?php echo $this->Form->input('serial_number', array('type' => 'hidden', 'class' => 'form-control')); ?>
		<?php echo $this->Form->input('registered', array('type'=> 'hidden', 'class' => 'form-control')); ?>
		<?php echo $this->Form->input('imei', array('class' => 'form-control')); ?>
		<?php echo $this->Form->input('name', array('class' => 'form-control')); ?>
		<?php echo $this->Form->input('asset_id', array('type'=>'hidden', 'class' => 'form-control')); ?>
		<?php echo $this->Form->input('ip_address', array('class' => 'form-control')); ?>
		

	</div>
	
	</fieldset>
<?php echo $this->Form->submit( 'Change ip address Device' , array('type' => 'submit', 'class' => 'btn btn-success '));?>	 
                        <?php echo $this->Form->end(); ?>
</div>
 </div>
            </div>
        </div>
    </div>
</div>	