<?php
echo $this->Html->addCrumb(" Reports", "/users/index");
echo $this->Html->addCrumb(' Devices Summary ');
?>

<div class="col-md-3">
    <div class="well">
        <ul class="nav nav-pills nav-stacked">
             <li><a href="/devices/dashboard">Connectivity Map</a></li>
            <li><a href="/incidents/index">Incident Management</a></li>
			<li><a href="/sims/sim_status">Sim Status</a></li>
			<li><a href="/customers/index">Customers Data</a></li>
            <li><a href="/assets/index">Assets Data</a></li>
			<li><a href="/devices/index">Devices Data</a></li>   
            <li><a href="/sims/sim_data">Sims Data</a></li>
			<li><a href="/sims/on_demand_sim_signal_strength">On demand Sim Signal Strength</a></li>
            <li><a href="/sims/on_demand_sim_connection_speed_test">Sim Connection Speed Test</a></li>
            <li><a href="/sims/on_demand_signal_quality_test">Signal Quality Test</a></li>
            <li><a href="/sims/on_demand_network_performance_and_bandwidth">Network performance and bandwidth usage</a></li>
            <li class="active"><a href="/users/index">Reports</a></li>
        </ul>
    </div><!-- well -->
</div><!-- col-md-2 -->
<div class="col-md-9">
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">

                <div class="col-xs-12">
<div class="devices index">

	<h3>Devices Summary</h3>
	
	<br /><br />
	<table id ="data-table" class="table style="margin-left:10px;">
	<thead>
			
			<th>pin</th>
		
			<th>serial_number</th>
			
			<th>imei</th>
			<th>ip_address</th>
			<th>name</th>
			<th>Asset</th>
			
	</thead>
	<?php foreach ($devices as $device): ?>
	<tr>
	
		<td><?php echo h($device['Device']['pin']); ?>&nbsp;</td>
		<td><?php echo h($device['Device']['serial_number']); ?>&nbsp;</td>
		<td><?php echo h($device['Device']['imei']); ?>&nbsp;</td>
		<td><?php echo h($device['Device']['ip_address']); ?>&nbsp;</td>
		<td><?php echo h($device['Device']['name']); ?>&nbsp;</td>
		
		<td>
			<?php echo $this->Html->link($device['Asset']['name'], array('controller' => 'assets', 'action' => 'view', $device['Asset']['id'])); ?>
		</td>
		
	</tr>
<?php endforeach; ?>
	</table>
	
</div>

 </div>
                </div>
            </div>
        </div>
    </div>	

