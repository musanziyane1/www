<?php
	echo $this->Html->addCrumb(" Provisioning", "/Devices/single_ip_address_allocation");
	echo $this->Html->addCrumb(' Batch IP Address Allocation');
?>
      <!-- Example row of columns -->
       <div class="col-md-3">	
    <div class="well">
			<ul class="nav nav-pills nav-stacked">
				<li><a href="/devices/single_ip_address_allocation">Single Allocation of Static IP Address</a></li>
				<li class="active"><a href="/devices/batch_ip_address_allocation">Batch Allocation of Static IP Address</a></li>
				<li><a href="/sims/vendor_sim_allocation">Vendor SIM Allocation</a></li>
				<li><a href="/devices/installation_report">Installation Report</a></li>
  			</ul>
        </div>
		</div>
        <div class="col-lg-9">
        
          <div class="panel panel-default">
			  <div class="panel-body">
			    <h3>Batch Allocation of Static IP Address</h3>
				<form role="form">
				 
				  <div class="form-group">
				  <table id ="data-table" class="table style="margin-left:10px;">
  				    <thead>
		
			<th>imei</th>
			<th>ip_address</th>
			<th>name</th>
		
			<th class="actions"><?php echo __('select'); ?></th>
	</thead>
	<?php foreach ($devices as $device): ?>
	<tr>
	
		
		<td><?php echo h($device['Device']['imei']); ?>&nbsp;</td>
		<td><?php echo h($device['Device']['ip_address']); ?>&nbsp;</td>
		<td><?php echo h($device['Device']['name']); ?>&nbsp;</td>
		
		
		<td class="actions">
        
       <input type="checkbox" name="vehicle" value="Bike" class="form-control">
        
        </td>
	</tr>
	
<?php endforeach; ?>

	</table>
			 <?php echo $this->Html->link(__('Deallocate ip address'), array('action' => 'edit', $device['Device']['id']), array('type' => 'submit', 'class' => 'btn btn-success ')); ?></div>

				</form>
			</div>
		</div>

       </div>
    