<?php
echo $this->Html->addCrumb(' Devices List ', '/devices/index');

?>

<div class="col-md-3">
    <div class="well">
        <ul class="nav nav-pills nav-stacked">
                <li><a href="/devices/dashboard">Connectivity Map</a></li>
                <li><a href="/incidents/index">Incident Management</a></li>
                <li><a href="/sims/sim_status">Sim Status</a></li>
				<li><a href="/customers/index">Customers Data</a></li>
				 <li><a href="/assets/index">Assets Data</a></li>
				<li><a href="/devices/index">Devices Data</a></li>   
                <li class="active"><a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Sim Data</a></li>
  
				<div id="collapseOne" class="panel-collapse collapse in">
				<div class="panel-body">
				<a href="/device/ip_address" class="active list-group-item">View Sim IP Address</span>
                        </a>
						<a href="/sims/index" class="list-group-item">Sim List</span>
                        </a>	
                        <a href="/sims/sim_stats" class="list-group-item">View SIM data usage and statistics</span>
                        </a>	
						
				</div>
				</div>
				<li><a href="/sims/on_demand_sim_signal_strength">On demand Sim Signal Strength</a></li>
				<li><a href="/sims/on_demand_sim_connection_speed_test">Sim Connection Speed Test</a></li>
				<li><a href="/sims/on_demand_signal_quality_test">Signal Quality Test</a></li>
				<li><a href="/sims/on_demand_network_performance_and_bandwidth">Network performance and bandwidth usage</a></li>
                <li><a href="/users/index">Reports</a></li>
				
            </ul>
    </div><!-- well -->
</div><!-- col-md-2 -->
<div class="col-md-9">
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">

                <div class="col-xs-12">
<div class="devices index">

	<h3>IP address List</h3>
	
	<table id ="data-table" class="table style="margin-left:10px;">
	<thead>
			
			<th>imei</th>
			<th>ip_address</th>
			<th>name</th>
			
			
	</thead>
	<?php foreach ($devices as $device): ?>
	<tr>
	
	
		
		<td><?php echo h($device['Device']['imei']); ?>&nbsp;</td>
		<td><?php echo h($device['Device']['ip_address']); ?>&nbsp;</td>
		<td><?php echo h($device['Device']['name']); ?>&nbsp;</td>
		
		
		
	</tr>
<?php endforeach; ?>
	</table>
	
</div>

 </div>
                </div>
            </div>
        </div>
    </div>	

