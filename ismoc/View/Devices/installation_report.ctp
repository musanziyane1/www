<?php
echo $this->Html->addCrumb(" Provisioning", "/Devices/single_ip_address_allocation");
echo $this->Html->addCrumb(' Installation Report');
?>
<!-- Example row of columns -->

<div class="col-md-3">	
    <div class="well">	
        <ul class="nav nav-pills nav-stacked">
            <li><a href="/devices/single_ip_address_allocation">Single Allocation of Static IP Address</a></li>
            <li><a href="/devices/batch_ip_address_allocation">Batch Allocation of Static IP Address</a></li>
            <li><a href="/sims/vendor_sim_allocation">Vendor SIM Allocation</a></li>
            <li class="active"><a href="/Devices/installation_report">Installation Report</a></li>
			
        </ul>
		
    </div>
</div>
<div class="col-lg-9">

    <div class="panel panel-default">
        <div class="panel-body">
            <h3>Installation Reports</h3>
            <form role="form">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Start Date</label>
                            <select class="form-control" id="start-date">
                                <option>JULY 2013</option>
                                <option>AUGUST 2013</option>
                                <option>SEPTEMBER 2013</option>
                                <option>OCTOBER 2013</option>
                                <option>NOVEMBER 2013</option>
                                <option>DECEMBER 2013</option>
                                <option>JANUARY 2014</option>
                                <option>FEBRUARY 2014</option>
                                <option>MARCH 2014</option>
                                <option>APRIL 2014</option>
                                <option>MAY 2014</option>
                                <option>JUNE 2014</option>
                                <option>JULY 2014</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6"> 
                        <div class="form-group">
                            <label>End Date</label>
                            <select class="form-control" id="end_date">
                                <option>JULY 2013</option>
                                <option>AUGUST 2013</option>
                                <option>SEPTEMBER 2013</option>
                                <option>OCTOBER 2013</option>
                                <option>NOVEMBER 2013</option>
                                <option>DECEMBER 2013</option>
                                <option>JANUARY 2014</option>
                                <option>FEBRUARY 2014</option>
                                <option>MARCH 2014</option>
                                <option>APRIL 2014</option>
                                <option>MAY 2014</option>
                                <option>JUNE 2014</option>
                                <option>JULY 2014</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label>Report Type</label>
                    <select class="form-control" id="type">
                        <option>New Installations</option>
                        <option>Poor Connectivity</option>
                        <option>Outstanding work orders</option>
                    </select>
                </div>

                <div class="form-group">
                  
					<a class="btn btn-success" onclick="myFunction()">Email Report</a>
                    <script>
                        function myFunction() {
							var report_type = document.getElementById("type").value;
							var date1 = document.getElementById("start-date").value;
							var date2 = document.getElementById("end_date").value;
                            var email = prompt("Please enter your email", "email@email.com");

                            if (email != null) {
                                document.getElementById("demo").innerHTML =
                                        alert("Your " + report_type + " Report for dates: " + date1 + " to "+ date2 +" was sent to " + email + " ! Please check your inbox");
                            }
                        }
                    </script>
                </div>


                <div class="form-group">

                </div>

        </div>				  			

        </form>
		<hr >
    </div>
</div>

       