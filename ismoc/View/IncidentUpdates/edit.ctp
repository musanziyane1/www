<div class="incidentUpdates form">
    <?php echo $this->Form->create('IncidentUpdate'); ?>
    <fieldset>
        <legend><?php echo __('Edit Incident Update'); ?></legend>
        <?php
        echo $this->Form->input('id');
        echo $this->Form->input('description');
        echo $this->Form->input('incident_type_id');
        echo $this->Form->input('severity_id');
        echo $this->Form->input('incident_status_id');
        echo $this->Form->input('technician_id');
        echo $this->Form->input('user_id');
        echo $this->Form->input('authorised_by');
        echo $this->Form->input('incident_id');
        ?>
    </fieldset>
    <?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
    <h3><?php echo __('Actions'); ?></h3>
    <ul>

        <li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('IncidentUpdate.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('IncidentUpdate.id'))); ?></li>
        <li><?php echo $this->Html->link(__('List Incident Updates'), array('action' => 'index')); ?></li>
        <li><?php echo $this->Html->link(__('List Incident Types'), array('controller' => 'incident_types', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('New Incident Type'), array('controller' => 'incident_types', 'action' => 'add')); ?> </li>
        <li><?php echo $this->Html->link(__('List Severities'), array('controller' => 'severities', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('New Severity'), array('controller' => 'severities', 'action' => 'add')); ?> </li>
        <li><?php echo $this->Html->link(__('List Incident Statuses'), array('controller' => 'incident_statuses', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('New Incident Status'), array('controller' => 'incident_statuses', 'action' => 'add')); ?> </li>
        <li><?php echo $this->Html->link(__('List Technicians'), array('controller' => 'technicians', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('New Technician'), array('controller' => 'technicians', 'action' => 'add')); ?> </li>
        <li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
        <li><?php echo $this->Html->link(__('List Incidents'), array('controller' => 'incidents', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('New Incident'), array('controller' => 'incidents', 'action' => 'add')); ?> </li>
    </ul>
</div>
