<div class="incidentUpdates index">
    <h2><?php echo __('Incident Updates'); ?></h2>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?php echo $this->Paginator->sort('id'); ?></th>
            <th><?php echo $this->Paginator->sort('description'); ?></th>
            <th><?php echo $this->Paginator->sort('incident_type_id'); ?></th>
            <th><?php echo $this->Paginator->sort('severity_id'); ?></th>
            <th><?php echo $this->Paginator->sort('incident_status_id'); ?></th>
            <th><?php echo $this->Paginator->sort('technician_id'); ?></th>
            <th><?php echo $this->Paginator->sort('user_id'); ?></th>
            <th><?php echo $this->Paginator->sort('created'); ?></th>
            <th><?php echo $this->Paginator->sort('modified'); ?></th>
            <th><?php echo $this->Paginator->sort('authorised_by'); ?></th>
            <th><?php echo $this->Paginator->sort('incident_id'); ?></th>
            <th class="actions"><?php echo __('Actions'); ?></th>
        </tr>
        <?php foreach ($incidentUpdates as $incidentUpdate): ?>
            <tr>
                <td><?php echo h($incidentUpdate['IncidentUpdate']['id']); ?>&nbsp;</td>
                <td><?php echo h($incidentUpdate['IncidentUpdate']['description']); ?>&nbsp;</td>
                <td>
                    <?php echo $this->Html->link($incidentUpdate['IncidentType']['description'], array('controller' => 'incident_types', 'action' => 'view', $incidentUpdate['IncidentType']['id'])); ?>
                </td>
                <td>
                    <?php echo $this->Html->link($incidentUpdate['Severity']['description'], array('controller' => 'severities', 'action' => 'view', $incidentUpdate['Severity']['id'])); ?>
                </td>
                <td>
                    <?php echo $this->Html->link($incidentUpdate['IncidentStatus']['description'], array('controller' => 'incident_statuses', 'action' => 'view', $incidentUpdate['IncidentStatus']['id'])); ?>
                </td>
                <td>
                    <?php echo $this->Html->link($incidentUpdate['Technician']['first_name'], array('controller' => 'technicians', 'action' => 'view', $incidentUpdate['Technician']['id'])); ?>
                </td>
                <td>
                    <?php echo $this->Html->link($incidentUpdate['User']['first_name'], array('controller' => 'users', 'action' => 'view', $incidentUpdate['User']['id'])); ?>
                </td>
                <td><?php echo h($incidentUpdate['IncidentUpdate']['created']); ?>&nbsp;</td>
                <td><?php echo h($incidentUpdate['IncidentUpdate']['modified']); ?>&nbsp;</td>
                <td><?php echo h($incidentUpdate['IncidentUpdate']['authorised_by']); ?>&nbsp;</td>
                <td>
                    <?php echo $this->Html->link($incidentUpdate['Incident']['description'], array('controller' => 'incidents', 'action' => 'view', $incidentUpdate['Incident']['id'])); ?>
                </td>
                <td class="actions">
                    <?php echo $this->Html->link(__('View'), array('action' => 'view', $incidentUpdate['IncidentUpdate']['id'])); ?>
                    <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $incidentUpdate['IncidentUpdate']['id'])); ?>
                    <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $incidentUpdate['IncidentUpdate']['id']), array(), __('Are you sure you want to delete # %s?', $incidentUpdate['IncidentUpdate']['id'])); ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </table>
    <p>
        <?php
        echo $this->Paginator->counter(array(
            'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
        ));
        ?>	</p>
    <div class="paging">
        <?php
        echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
        echo $this->Paginator->numbers(array('separator' => ''));
        echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
        ?>
    </div>
</div>
<div class="actions">
    <h3><?php echo __('Actions'); ?></h3>
    <ul>
        <li><?php echo $this->Html->link(__('New Incident Update'), array('action' => 'add')); ?></li>
        <li><?php echo $this->Html->link(__('List Incident Types'), array('controller' => 'incident_types', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('New Incident Type'), array('controller' => 'incident_types', 'action' => 'add')); ?> </li>
        <li><?php echo $this->Html->link(__('List Severities'), array('controller' => 'severities', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('New Severity'), array('controller' => 'severities', 'action' => 'add')); ?> </li>
        <li><?php echo $this->Html->link(__('List Incident Statuses'), array('controller' => 'incident_statuses', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('New Incident Status'), array('controller' => 'incident_statuses', 'action' => 'add')); ?> </li>
        <li><?php echo $this->Html->link(__('List Technicians'), array('controller' => 'technicians', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('New Technician'), array('controller' => 'technicians', 'action' => 'add')); ?> </li>
        <li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
        <li><?php echo $this->Html->link(__('List Incidents'), array('controller' => 'incidents', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('New Incident'), array('controller' => 'incidents', 'action' => 'add')); ?> </li>
    </ul>
</div>
