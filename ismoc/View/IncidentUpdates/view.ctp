<div class="incidentUpdates view">
    <h2><?php echo __('Incident Update'); ?></h2>
    <dl>
        <dt><?php echo __('Id'); ?></dt>
        <dd>
            <?php echo h($incidentUpdate['IncidentUpdate']['id']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Description'); ?></dt>
        <dd>
            <?php echo h($incidentUpdate['IncidentUpdate']['description']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Incident Type'); ?></dt>
        <dd>
            <?php echo $this->Html->link($incidentUpdate['IncidentType']['description'], array('controller' => 'incident_types', 'action' => 'view', $incidentUpdate['IncidentType']['id'])); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Severity'); ?></dt>
        <dd>
            <?php echo $this->Html->link($incidentUpdate['Severity']['description'], array('controller' => 'severities', 'action' => 'view', $incidentUpdate['Severity']['id'])); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Incident Status'); ?></dt>
        <dd>
            <?php echo $this->Html->link($incidentUpdate['IncidentStatus']['description'], array('controller' => 'incident_statuses', 'action' => 'view', $incidentUpdate['IncidentStatus']['id'])); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Technician'); ?></dt>
        <dd>
            <?php echo $this->Html->link($incidentUpdate['Technician']['first_name'], array('controller' => 'technicians', 'action' => 'view', $incidentUpdate['Technician']['id'])); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('User'); ?></dt>
        <dd>
            <?php echo $this->Html->link($incidentUpdate['User']['first_name'], array('controller' => 'users', 'action' => 'view', $incidentUpdate['User']['id'])); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Created'); ?></dt>
        <dd>
            <?php echo h($incidentUpdate['IncidentUpdate']['created']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Modified'); ?></dt>
        <dd>
            <?php echo h($incidentUpdate['IncidentUpdate']['modified']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Authorised By'); ?></dt>
        <dd>
            <?php echo h($incidentUpdate['IncidentUpdate']['authorised_by']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Incident'); ?></dt>
        <dd>
            <?php echo $this->Html->link($incidentUpdate['Incident']['description'], array('controller' => 'incidents', 'action' => 'view', $incidentUpdate['Incident']['id'])); ?>
            &nbsp;
        </dd>
    </dl>
</div>
<div class="actions">
    <h3><?php echo __('Actions'); ?></h3>
    <ul>
        <li><?php echo $this->Html->link(__('Edit Incident Update'), array('action' => 'edit', $incidentUpdate['IncidentUpdate']['id'])); ?> </li>
        <li><?php echo $this->Form->postLink(__('Delete Incident Update'), array('action' => 'delete', $incidentUpdate['IncidentUpdate']['id']), array(), __('Are you sure you want to delete # %s?', $incidentUpdate['IncidentUpdate']['id'])); ?> </li>
        <li><?php echo $this->Html->link(__('List Incident Updates'), array('action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('New Incident Update'), array('action' => 'add')); ?> </li>
        <li><?php echo $this->Html->link(__('List Incident Types'), array('controller' => 'incident_types', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('New Incident Type'), array('controller' => 'incident_types', 'action' => 'add')); ?> </li>
        <li><?php echo $this->Html->link(__('List Severities'), array('controller' => 'severities', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('New Severity'), array('controller' => 'severities', 'action' => 'add')); ?> </li>
        <li><?php echo $this->Html->link(__('List Incident Statuses'), array('controller' => 'incident_statuses', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('New Incident Status'), array('controller' => 'incident_statuses', 'action' => 'add')); ?> </li>
        <li><?php echo $this->Html->link(__('List Technicians'), array('controller' => 'technicians', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('New Technician'), array('controller' => 'technicians', 'action' => 'add')); ?> </li>
        <li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
        <li><?php echo $this->Html->link(__('List Incidents'), array('controller' => 'incidents', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('New Incident'), array('controller' => 'incidents', 'action' => 'add')); ?> </li>
    </ul>
</div>
