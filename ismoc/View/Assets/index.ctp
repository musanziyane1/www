<?php
echo $this->Html->addCrumb(' Assets List ', '/assets/index');
?>

<div class="col-md-3">
    <div class="well">
        <ul class="nav nav-pills nav-stacked">
            <li><a href="/devices/dashboard">Connectivity Map</a></li>
            <li><a href="/incidents/index">Incident Management</a></li>
            <li><a href="/sims/sim_status">Sim Status</a></li>
            <li><a href="/customers/index">Customers Data</a></li>
            <li class="active"><a href="/assets/index">Assets Data</a></li>
            <li><a href="/devices/index">Devices Data</a></li>
            <li><a href="/sims/sim_data">Sims Data</a></li>
            <li><a href="/sims/on_demand_sim_signal_strength">On demand Sim Signal Strength</a></li>
            <li><a href="/sims/on_demand_sim_connection_speed_test">Sim Connection Speed Test</a></li>
            <li><a href="/sims/on_demand_signal_quality_test">Signal Quality Test</a></li>
            <li><a href="/sims/on_demand_network_performance_and_bandwidth">Network performance and bandwidth usage</a></li>
            <li><a href="/users/index">Reports</a></li>
        </ul>
    </div><!-- well -->
</div><!-- col-md-2 -->
<div class="col-md-9">
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">

                <div class="col-xs-12">

                    <h3>Assets List</h3>
                    <?php echo $this->Html->link(__('New Asset'), array('action' => 'add'), array('type' => 'submit', 'class' => 'btn btn-success ')); ?><br /><br />
                    <div class='table-responsive'>
                        <div class="assets index">

                            <table id ="data-table" class="table style="margin-left:10px;">
                                   <thead>

                                <th>name</th>
                                <th>created</th>
                                <th>modified</th>
                                <th class="actions"><?php echo __('Actions'); ?></th>
                                </thead>
                                <?php foreach ($assets as $asset): ?>
                                    <tr>

                                        <td><?php echo h($asset['Asset']['name']); ?>&nbsp;</td>
                                        <td><?php echo h($asset['Asset']['created']); ?>&nbsp;</td>
                                        <td><?php echo h($asset['Asset']['modified']); ?>&nbsp;</td>
                                        <td class="actions">
                                            <?php echo $this->Html->link(__('View'), array('action' => 'view', $asset['Asset']['id']), array('type' => 'submit', 'class' => 'btn btn-success ')); ?>
                                            <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $asset['Asset']['id']), array('type' => 'submit', 'class' => 'btn btn-success ')); ?>
                                            <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $asset['Asset']['id']), array('type' => 'submit', 'class' => 'btn btn-success '), __('Are you sure you want to delete # %s?', $asset['Asset']['id'])); ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </table>


                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>	

