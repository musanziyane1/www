<?php
echo $this->Html->addCrumb(' Assets List ', '/assets/index');
echo $this->Html->addCrumb(" Add new Asset");
?>
<div class="col-md-3">
    <div class="well">
        <ul class="nav nav-pills nav-stacked">
            <li><a href="/devices/dashboard">Connectivity Map</a></li>
            <li><a href="/incidents/index">Incident Management</a></li>
            <li><a href="/sims/sim_status">Sim Status</a></li>
            <li><a href="/customers/index">Customers Data</a></li>
            <li class="active"><a href="/assets/index">Assets Data</a></li>
            <li><a href="/devices/index">Devices Data</a></li>   
            <li><a href="/sims/sim_data">Sims Data</a></li>
            <li><a href="/sims/on_demand_sim_signal_strength">On demand Sim Signal Strength</a></li>
            <li><a href="/sims/on_demand_sim_connection_speed_test">Sim Connection Speed Test</a></li>
            <li><a href="/sims/on_demand_signal_quality_test">Signal Quality Test</a></li>
            <li><a href="/sims/on_demand_network_performance_and_bandwidth">Network performance and bandwidth usage</a></li>
            <li><a href="/users/index">Reports</a></li>
        </ul>
    </div><!-- well -->
</div><!-- col-md-2 -->
<div class="col-md-9">
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">

                <div class="col-xs-12">
                    <div class="assets form">
                        <?php echo $this->Form->create('Asset'); ?>
                        <fieldset>
                            <legend><?php echo __('Add Asset'); ?></legend>
                            <div class="form-group">
                                <?php echo $this->Form->input('name', array('class' => 'form-control')); ?>
                            </div>
                        </fieldset>
                        <?php echo $this->Form->submit('Add Asset', array('type' => 'submit', 'class' => 'btn btn-success ')); ?>	 
                        <?php echo $this->Form->end(); ?>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>	
