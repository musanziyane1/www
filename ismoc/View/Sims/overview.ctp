<?php echo $this->Html->addCrumb(" SIMS Overview"); ?>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">

                    <h4>SIMS Overview</h4>

                    <a href="#" class="btn btn-primary" id="btn-success" >Add group incident</a><br /><br/>
                    <div class='table-responsive'>
                        <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th><?php echo $this->Paginator->sort('id'); ?></th>
                                    <th><?php echo $this->Paginator->sort('msisdn'); ?></th>
                                    <th><?php echo $this->Paginator->sort('iccid'); ?></th>
                                    <th><?php echo $this->Paginator->sort('customer_id'); ?></th>
                                    <td></td>
                                    <td></td>

                                </tr>
                            </thead>
                            <thead>
                                <tr>
                                    <td><?php echo '<b>id</b>'; ?></td>
                                    <td><?php echo'<b>msisdn</b>'; ?></td>
                                    <td><?php echo '<b>iccid</b>'; ?></td>

                                    <td><?php echo '<b>Customer ID</b>'; ?></td>


                                    <td><?php echo '<b>Select</b>' ?></td>
                                    <td><b><?php echo 'Action'; ?></b></td>
                                </tr>
                            </thead>
                            <?php foreach ($sims as $sim): ?>
                                <tr>
                                    <td><?php echo h($sim['Sim']['id']); ?></td>
                                    <td><?php echo h($sim['Sim']['msisdn']); ?></td>
                                    <td><?php echo h($sim['Sim']['iccid']); ?></td>

                                    <td>
                                        <?php echo $this->Html->link($sim['Customer']['first_name'], array('controller' => 'customers', 'action' => 'view', $sim['Customer']['id'])); ?>
                                    </td>

                                    <td><input type="checkbox"></td>
                                    <td><?php echo $this->Html->link(__('View'), array('action' => 'view', $sim['Sim']['id'])); ?></td>

                                </tr>
                            <?php endforeach; ?>
                        </table>


                    </div>
                </div>


            </div>
        </div>
    </div>
