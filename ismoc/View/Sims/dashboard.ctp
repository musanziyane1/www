<script>
    $(function() {
        $('.cities').hide();
        $('#district').change(function() {
            $('.cities').hide();
            $('#' + $(this).val()).show();
        });
    });
</script>

<div class="row">
    <div class="col-lg-16">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="col-lg-6"><br />
                    <div class="btn-group btn-group-lg">

                        <a type="button" class="btn btn-primary" href="/sims/overview"> Sims</a>
                        <a type="button" class="btn btn-primary" href="/customers/overview">Customers</a>
                        <a type="button" class="btn btn-primary" href="/incidents/overview">Incidents</a>
                        <a type="button" class="btn btn-primary" href="/technicians/overview">Technicians</a>
                    </div>
                    <br />
                    <br />
                    <br /><br />
                    <ul class="list-group">
                        <a href="#" class="list-group-item">
                            <span class="badge">0</span>
                            Online Sims</a>

                        <a href="#" class="list-group-item">
                            <span class="badge">0</span>
                            Offline Sims
                        </a>

                        <a href="#" class="list-group-item">
                            <span class="badge"><?php echo $openedIncidents; ?></span>
                            In Progress Incidents
                            </li>
                            <a href="#" class="list-group-item">
                                <span class="badge"><?php echo$inprogressIncidents; ?></span>
                                Resolved Incidents
                            </a>
                            <a href="#" class="list-group-item">
                                <span class="badge"><?php echo $escalatedIncidents; ?></span>
                                Escalated Incidents
                            </a>

                            <a href="#" class="list-group-item">
                                <span class="badge"><?php echo $closedIncidents; ?></span>
                                Closed Incidents
                            </a>
                    </ul>

                </div>
                <div class="col-lg-6">
                    <b>Filter by District:</b>
                    <select id="district" class="form-control">
                        <b>Filter by City:</b>
                        <option value="joburg">City of Johannesburg Metropolitan Municipality</option>
                        <option value="tshwane">City of Tshwane Metropolitan Municipality</option>
                        <option value="ekurhuleni">Ekurhuleni Metropolitan Municipality</option>
                        <option value="sedibeng">Sedibeng District Municipality</option>
                        <option value="westrand">West Rand District Municipality</option>

                    </select>

                    <br />
                    <select id="joburg" class="cities form-control" style="display:none">

                        <option value="Alexandra">Alexandra</option>
                        <option value="Chartwell">Chartwell</option>
                        <option value="Dainfern">Dainfern</option>
                        <option value="Diepsloot">Diepsloot</option>
                        <option value="Drie Ziek">Drie Ziek</option>
                        <option value="Ebony Park">Ebony Park</option>
                        <option value="Ennerdale">Ennerdale</option>
                        <option value="Farmall">Farmall</option>
                        <option value="Itsoseng">Itsoseng</option>
                        <option value="Ivory Park">Ivory Park</option>
                        <option value="Johannesburg">Johannesburg</option>
                        <option value="Kaalfontein">Kaalfontein</option>
                        <option value="Kagiso">Kagiso</option>
                        <option value="Kanana Park">Kanana Park</option>
                        <option value="Lakeside">Lakeside</option>
                        <option value="Lanseria">Lanseria</option>
                        <option value="Lawley">Lawley</option>
                        <option value="Lehae">Lehae</option>
                        <option value="Lenasia">Lenasia</option>
                        <option value="Lenasia Southe">Lenasia South</option>
                        <option value="Lucky 7">Lucky 7</option>
                        <option value="Malatjie">Malatjie</option>
                        <option value="Mayibuye">Mayibuye</option>
                        <option value="Midrand">Midrand</option>
                        <option value="Millgate Farm">Millgate Farm</option>
                        <option value="Orange Farm">Orange Farm</option>
                        <option value="Poortjie">Poortjie</option>
                        <option value="Rabie Ridge">Rabie Ridge</option>
                        <option value="Randburg">Randburg</option>
                        <option value="Randfontein">Randfontein</option>
                        <option value="Rietfontein">Rietfontein</option>
                        <option value="Roodepoort">Roodepoort</option>
                        <option value="Sandton">Sandton</option>
                        <option value="Soweto">Soweto</option>
                        <option value="Stretford">Stretford</option>
                        <option value="Tshepisong">Tshepisong</option>
                        <option value="Vlakfontein">Vlakfontein</option>
                        <option value="Zakariyya Park">Zakariyya Park</option>
                        <option value="Zevenfonteine">Zevenfontein</option>

                    </select>
                    <select id="tshwane" class="cities form-control" style="display:none">
                        <option value="Akasia">Akasia</option>
                        <option value="Atteridgeville">Atteridgeville</option>
                        <option value="Baviaanspoort">Baviaanspoort</option>
                        <option value="Bon Accord">Bon Accord</option>
                        <option value="Boschkop">Boschkop</option>
                        <option value="Bronkhorstspruit">Bronkhorstspruit</option>
                        <option value="Bultfontein">Bultfontein</option>
                        <option value="Centurion">Centurion</option>
                        <option value="Cullinan">Cullinan</option>
                        <option value="Dilopye">Dilopye</option>
                        <option value="Donkerhoek">Donkerhoek</option>
                        <option value="Eersterus">Eersterus</option>
                        <option value="Ekangala">Ekangala</option>
                        <option value="Ga-Rankuwa">Ga-Rankuwa</option>
                        <option value="Haakdoornboom">Haakdoornboom</option>
                        <option value="Hammanskraal">Hammanskraal</option>
                        <option value="Hebron">Hebron</option>
                        <option value="Kameeldrift">Kameeldrift</option>
                        <option value="Kekana Garden">Kekana Garden</option>
                        <option value="Kungwini">Kungwini</option>
                        <option value="Laudium">Laudium</option>
                        <option value="Mabopane">Mabopane</option>
                        <option value="Majaneng">Majaneng</option>
                        <option value="Mamelodi">Mamelodi</option>
                        <option value="Mandela Village">Mandela Village</option>
                        <option value="Marokolong">Marokolong</option>
                        <option value="Mashemong">Mashemong</option>
                        <option value="Mooiplaas">Mooiplaas</option>
                        <option value="Nellmapius">Nellmapius</option>
                        <option value="New Eersterus">New Eersterus</option>
                        <option value="Olievenhoutbos">Olievenhoutbos</option>
                        <option value="Onverwacht">Onverwacht</option>
                        <option value="Pretoria">Pretoria</option>
                        <option value="Ramotse">Ramotse</option>
                        <option value="Rayton">Rayton</option>
                        <option value="Refilwe">Refilwe</option>
                        <option value="Rethabiseng">Rethabiseng</option>
                        <option value="Roodepoort B">Roodepoort B</option>
                        <option value="Saulsville">Saulsville</option>
                        <option value="Soshanguve">Soshanguve</option>
                        <option value="Soutpan">Soutpan</option>
                        <option value="Stinkwater">Stinkwater</option>
                        <option value="Suurman">Suurman</option>
                        <option value="Temba">Temba</option>
                        <option value="Thembisile">Thembisile</option>
                        <option value="Tierpoort">Tierpoort</option>
                        <option value="Tsebe">Tsebe</option>
                        <option value="Tshwane NU">Tshwane NU</option>
                        <option value="Vaalbank">Vaalbank</option>
                        <option value="Waterval">Waterval</option>
                        <option value="Winterveld">Winterveld</option>
                        <option value="Zithobeni">Zithobeni</option>
                        <option value="Zwavelpoort">Zwavelpoort</option>


                    </select>

                    </select>
                    <select id="ekurhuleni" class="cities form-control" style="display:none">
                        <option value="map">Alberton</option>
                        <option value="map-online">Bapsfontein</option>
                        <option value="map">Bedfordview</option>
                        <option value="map-online">Benoni</option>
                        <option value="map">Boschkop</option>
                        <option value="map-online">Boksburg</option>
                        <option value="map">Brakpan</option>
                        <option value="map-online">Cerutiville</option>
                        <option value="map">Chief Albert Lithuli Park</option>
                        <option value="map-online">Daveyton</option>
                        <option value="map">Duduza</option>
                        <option value="map-online">Dukathole</option>
                        <option value="map">Edenvale</option>
                        <option value="map-online">Etwatwa</option>
                        <option value="map">Germiston</option>
                        <option value="map-online">Katlehong</option>
                        <option value="map">Kempton Park</option>
                        <option value="map-online">KwaThema</option>
                        <option value="map">Kekana Garden</option>
                        <option value="map-online">Kungwini</option>
                        <option value="map">Laudium</option>
                        <option value="map-online">Mabopane</option>
                        <option value="map">Lindelani Village</option>
                        <option value="map-online">Midrand</option>
                        <option value="map">Nigel</option>
                        <option value="map-online">Reiger Park</option>
                        <option value="map">Springs</option>
                        <option value="map-online">Tembisa</option>
                        <option value="map">Thokoza</option>
                        <option value="map-online">Tsakane</option>
                        <option value="map">Vosloorus</option>
                        <option value="map-online">Wattville</option>


                    </select>


                    <select id="sedibeng" class="cities form-control" style="display:none">
                        <option value="map">Boipatong</option>
                        <option value="map-online">Bophelong</option>
                        <option value="map">Devon</option>
                        <option value="map-online">Evaton</option>
                        <option value="map-online">Midvaal</option> 
                        <option value="map-online">Impumelelo</option>
                        <option value="map">Ratanda</option>
                        <option value="map-online">Evaton</option>
                        <option value="map-online">Meyerton</option>
                        <option value="map">Randvaal</option>
                        <option value="map">Sharpeville</option>
                        <option value="map">Suikerbosrand Nature Reserve</option>
                        <option value="map">Tshepiso</option>
                        <option value="map">Vaal Marina</option>
                        <option value="map">Vanderbijlpark</option>
                        <option value="map">Vereeniging</option>
                        <option value="map">Walkerville</option>

                    </select>

                    <select id="westrand" class="cities form-control" style="display:none">
                        <option value="Kagiso">Kagiso</option>
                        <option value="Krugersdorp">Krugersdorp</option>
                        <option value="Magaliesburg">Magaliesburg</option>
                        <option value="Mogale City">Mogale City</option>
                        <option value="Muldersdrift">Muldersdrift</option> 
                        <option value="Munsieville">Munsieville</option>
                        <option value="map">Orient Hills</option>
                    
                        <option value="Rietvallei">Rietvallei</option>
                        <option value="Blybank">Blybank</option>
                        <option value="Blyvooruitzicht">Blyvooruitzicht</option>
                        <option value="Carletonville">Carletonville</option>
                        <option value="Deelkraal">Deelkraal</option>
                        <option value="Doornfontein">Doornfontein</option>
                        <option value="East Driefontein Mine">East Driefontein Mine</option>
                        <option value="Elandsfontein">Elandsfontein</option>
                        <option value="Elandsrand">Elandsrand</option>
                        <option value="Elandsridge">Elandsridge</option>
                        <option value="Fochville">Fochville</option>
                        <option value="Khutsong">Khutsong</option>

                        <option value="Kokosi">Kokosi</option>
                        <option value="Leeupoort">Leeupoort</option>
                        <option value="Letsatsing">Letsatsing</option>
                        <option value="Oberholzer">Oberholzer</option>
                        <option value="Phomolong">Phomolong</option>
                        <option value="Wedela">Wedela</option>
                        <option value="Welverdiend">Welverdiend</option>
                        <option value="Westdriefontein">Westdriefontein</option>
                        <option value="Western Deep Levels Mine">Western Deep Levels Mine</option>
                        <option value="Bhongweni">Bhongweni</option>
                        <option value="Brandvlei">Brandvlei</option>
                        <option value="Mohlakeng">Mohlakeng</option>
                        <option value="Panvlak Gold Mine">Panvlak Gold Mine</option>
                        <option value="Randfontein">Randfontein</option>
                        <option value="Toekomsrus">Toekomsrus</option>
                        <option value="Zenzele">Zenzele</option>
                        <option value="Bekkersdal">Bekkersdal</option>
                        <option value="Cooke Mine">Cooke Mine</option>
                        <option value="Elsburg Gold Mine">Elsburg Gold Mine</option>
                        <option value="Etlebeni">Etlebeni</option>
                        <option value="Glen Harvie">Glen Harvie</option>
                        <option value="Hills Haven">Hills Haven</option>
                        <option value="Johannesburg">Johannesburg</option>
                        <option value="Kloof Gold Mine">Kloof Gold Mine</option>
                        <option value="Leeudoorn Mine">Leeudoorn Mine</option>
                        <option value="Libanon Gold Mine">Libanon Gold Mine</option>
                        <option value="Modderfontein">Modderfontein</option>
                        <option value="Panvlak Gold Mine">Panvlak Gold Mine</option>
                        <option value="Randfontein Mine">Randfontein Mine</option>
                        <option value="Venterspost">Venterspost</option>
                        <option value="Waterpan">Waterpan</option>
                        <option value="Westonaria">Westonaria</option>

                    </select>
                    <br />

                    <div id="map" class="maps" style="height:255px"></div>
                    
                    <script type="text/javascript">
                        // Define your locations: HTML content for the info window, latitude, longitude
                        var locations = [
                        <?php foreach ($sims as $sim): ?>
                            ['<h5><?php echo h($sim['Sim']['msisdn']); ?> - <?php echo $this->Html->link(__('View'), array('action' => 'view', $sim['Sim']['id'])); ?>  </h5>',<?php echo h($sim['Sim']['latitude']); ?>, <?php echo h($sim['Sim']['longitude']); ?>],    
                            
                        <?php endforeach; ?>
                        ];

                        // Setup the different icons and shadows
                        var iconURLPrefix = 'http://maps.google.com/mapfiles/ms/icons/';

                        var icons = [
                            iconURLPrefix + 'red-dot.png'
                        ]
                        var icons_length = icons.length;


                        var shadow = {
                            anchor: new google.maps.Point(15, 33),
                            url: iconURLPrefix + 'msmarker.shadow.png'
                        };

                        var  SelectedValue = "null";
                        var map = new google.maps.Map(document.getElementById('map'), {
                            
                            zoom: 8,
                            center: new google.maps.LatLng(-26.2145598, 27.964365),
                            mapTypeId: google.maps.MapTypeId.ROADMAP,
                            mapTypeControl: false,
                            streetViewControl: false,
                            panControl: false,
                            zoomControlOptions: {
                            position: google.maps.ControlPosition.LEFT_BOTTOM
                            }
                        });
                        
                        //Main Event Listener Start
                        district.onchange = function() {
                        SelectedValue = district.value;
                        
                        
                        if( SelectedValue == "joburg" ){
                        
                        var map = new google.maps.Map(document.getElementById('map'), {
                            
                            zoom: 11,
                            center: new google.maps.LatLng(-26.2145598, 27.964365),
                            mapTypeId: google.maps.MapTypeId.ROADMAP,
                            mapTypeControl: false,
                            streetViewControl: false,
                            panControl: false,
                            zoomControlOptions: {
                            position: google.maps.ControlPosition.LEFT_BOTTOM
                            }
                        });
                        
                        } else if ( SelectedValue == "tshwane" ){
                        
                        var map = new google.maps.Map(document.getElementById('map'), {
                            
                            zoom: 11,
                            center: new google.maps.LatLng(-25.7586499, 28.219682),
                            mapTypeId: google.maps.MapTypeId.ROADMAP,
                            mapTypeControl: false,
                            streetViewControl: false,
                            panControl: false,
                            zoomControlOptions: {
                            position: google.maps.ControlPosition.LEFT_BOTTOM
                            }
                        });
                        
                        } else if ( SelectedValue == "ekurhuleni" ){
                        
                        var map = new google.maps.Map(document.getElementById('map'), {
                            
                            zoom: 11,
                            center: new google.maps.LatLng(-26.1314182, 28.3416986),
                            mapTypeId: google.maps.MapTypeId.ROADMAP,
                            mapTypeControl: false,
                            streetViewControl: false,
                            panControl: false,
                            zoomControlOptions: {
                            position: google.maps.ControlPosition.LEFT_BOTTOM
                            }
                        });
                        
                        } else if ( SelectedValue == "sedibeng" ){
                        
                        var map = new google.maps.Map(document.getElementById('map'), {
                            
                            zoom: 11,
                            center: new google.maps.LatLng(-26.6311042, 28.0840407),
                            mapTypeId: google.maps.MapTypeId.ROADMAP,
                            mapTypeControl: false,
                            streetViewControl: false,
                            panControl: false,
                            zoomControlOptions: {
                            position: google.maps.ControlPosition.LEFT_BOTTOM
                            }
                        });
                        
                        } else {
                        
                        var map = new google.maps.Map(document.getElementById('map'), {
                            
                            zoom: 11,
                            center: new google.maps.LatLng(-26.0759947, 27.7666049),
                            mapTypeId: google.maps.MapTypeId.ROADMAP,
                            mapTypeControl: false,
                            streetViewControl: false,
                            panControl: false,
                            zoomControlOptions: {
                            position: google.maps.ControlPosition.LEFT_BOTTOM
                            }
                        });
                        
                        
                        }
                       
                       
                         var infowindow = new google.maps.InfoWindow({
                            width:200,
                            height:200
                            
                        });

                        var marker;
                        var markers = new Array();

                        var iconCounter = 0;

                         // Add the markers and infowindows to the map
                        for (var i = 0; i < locations.length; i++) {  
                        marker = new google.maps.Marker({
                        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                        map: map,
                        icon : icons[iconCounter],
                        shadow: shadow
                        });

                        markers.push(marker);

                        google.maps.event.addListener(marker, 'click', (function(marker, i) {
                        return function() {
                        infowindow.setContent(locations[i][0]);
                        infowindow.open(map, marker);
                    
                        }
                        })(marker, i));
      
                        iconCounter++;
                            // We only have a limited number of possible icon colors, so we may have to restart the counter
                        if(iconCounter >= icons_length){
                        iconCounter = 0;
                        }
                        }
                        
                        }
                        
                       
                        
                        
                        var infowindow = new google.maps.InfoWindow({
                            width:200,
                            height:200
                            
                        });

                        var marker;
                        var markers = new Array();

                        var iconCounter = 0;

                         // Add the markers and infowindows to the map
                        for (var i = 0; i < locations.length; i++) {  
                        marker = new google.maps.Marker({
                        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                        map: map,
                        icon : icons[iconCounter],
                        shadow: shadow
                        });

                        markers.push(marker);

                        google.maps.event.addListener(marker, 'click', (function(marker, i) {
                        return function() {
                        infowindow.setContent(locations[i][0]);
                        infowindow.open(map, marker);
                    
                        }
                        })(marker, i));
      
                        iconCounter++;
                            // We only have a limited number of possible icon colors, so we may have to restart the counter
                        if(iconCounter >= icons_length){
                        iconCounter = 0;
                        }
                        }
                        
                    </script> 

                </div>
            </div>
        </div>
    </div>
</div>


<div class="row">
   
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="col-lg-8">
                    <h4>Recent Incidents</h4>

                    <div class='table-responsive'>
                        <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <td><b>Description</b></td>
                                    <td><b>Type</b></td>
                                    <td><b>Status</b></td>
                                    <td><b>Logged date</b></td>
                                </tr>
                            </thead>



                            <tbody>
                                <?php foreach ($incidents as $incident): ?>
                                    <tr>
                                        <td><?php echo h($incident['Incident']['description']); ?></td>
                                        <td><?php echo h($incident['IncidentType']['description']); ?></td>
                                        <td><?php echo h($incident['IncidentStatus']['description']); ?></td>
                                        <td><?php echo h($incident['Incident']['created']); ?></td>

                                    </tr>


                                </tbody>
                            <?php endforeach; ?>
                        </table>
                    </div>
                </div>

                <div class="col-lg-4">
                    <h4>Total Assets</h4>
                    <br />
                    <ul class="list-group">

                        <a href="#" class="list-group-item">
                            <span class="badge"><?php echo $totalSims; ?></span>
                            Total Sims
                        </a>


                        <a href="#" class="list-group-item">
                            <span class="badge"><?php echo $totalCustomers; ?></span>
                            Total Customers
                        </a>

                        <a href="#" class="list-group-item">
                            <span class="badge"><?php echo $totalTechnicians; ?></span>
                            Total Technicians
                        </a>

                        <a href="#" class="list-group-item">
                            <span class="badge"><?php echo $totalIncidents; ?></span>
                            Total Incidents
                        </a>
                    </ul>
                </div>
            </div>
        </div>
  
</div>
