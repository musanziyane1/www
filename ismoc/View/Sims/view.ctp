<?php echo $this->Html->addCrumb(" SIMS Overview", "/sims/overview"); ?>
<?php echo $this->Html->addCrumb("View"); ?>
<style>
    .navbar a.navbar-brand {padding: 2px 15px 2px; }
</style>

<script>

    $(function() {

        $('#chart2').highcharts({
            chart: {
                type: 'gauge',
                plotBackgroundColor: null,
                plotBackgroundImage: null,
                plotBorderWidth: 0,
                plotShadow: false
            },
            title: {
                text: 'Connection Speed Test'
            },
            pane: {
                startAngle: -150,
                endAngle: 150,
                background: [{
                        backgroundColor: {
                            linearGradient: {x1: 0, y1: 0, x2: 0, y2: 1},
                            stops: [
                                [0, '#FFF'],
                                [1, '#333']
                            ]
                        },
                        borderWidth: 0,
                        outerRadius: '109%'
                    }, {
                        backgroundColor: {
                            linearGradient: {x1: 0, y1: 0, x2: 0, y2: 1},
                            stops: [
                                [0, '#333'],
                                [1, '#FFF']
                            ]
                        },
                        borderWidth: 1,
                        outerRadius: '107%'
                    }, {
                        // default background
                    }, {
                        backgroundColor: '#DDD',
                        borderWidth: 0,
                        outerRadius: '105%',
                        innerRadius: '103%'
                    }]
            },
            // the value axis
            yAxis: {
                min: 0,
                max: 200,
                minorTickInterval: 'auto',
                minorTickWidth: 1,
                minorTickLength: 10,
                minorTickPosition: 'inside',
                minorTickColor: '#666',
                tickPixelInterval: 30,
                tickWidth: 2,
                tickPosition: 'inside',
                tickLength: 10,
                tickColor: '#666',
                labels: {
                    step: 2,
                    rotation: 'auto'
                },
                title: {
                    text: 'mb/s'
                },
                plotBands: [{
                        from: 0,
                        to: 120,
                        color: '#55BF3B' // green
                    }, {
                        from: 120,
                        to: 160,
                        color: '#DDDF0D' // yellow
                    }, {
                        from: 160,
                        to: 200,
                        color: '#DF5353' // red
                    }]
            },
            series: [{
                    name: 'Speed',
                    data: [80],
                    tooltip: {
                        valueSuffix: ' mb/s'
                    }
                }]

        },
        // Add some life
        function(chart) {
            if (!chart.renderer.forExport) {
                setInterval(function() {
                    var point = chart.series[0].points[0],
                            newVal,
                            inc = Math.round((Math.random() - 0.5) * 20);

                    newVal = point.y + inc;
                    if (newVal < 0 || newVal > 200) {
                        newVal = point.y - inc;
                    }

                    point.update(newVal);

                }, 3000);
            }
        });
    });

</script>
<script>

    $(function() {

        $('#chart1').highcharts({
            chart: {
                type: 'gauge',
                plotBackgroundColor: null,
                plotBackgroundImage: null,
                plotBorderWidth: 0,
                plotShadow: false
            },
            title: {
                text: 'Network Perfomance Test'
            },
            pane: {
                startAngle: -150,
                endAngle: 150,
                background: [{
                        backgroundColor: {
                            linearGradient: {x1: 0, y1: 0, x2: 0, y2: 1},
                            stops: [
                                [0, '#FFF'],
                                [1, '#333']
                            ]
                        },
                        borderWidth: 0,
                        outerRadius: '109%'
                    }, {
                        backgroundColor: {
                            linearGradient: {x1: 0, y1: 0, x2: 0, y2: 1},
                            stops: [
                                [0, '#333'],
                                [1, '#FFF']
                            ]
                        },
                        borderWidth: 1,
                        outerRadius: '107%'
                    }, {
                        // default background
                    }, {
                        backgroundColor: '#DDD',
                        borderWidth: 0,
                        outerRadius: '105%',
                        innerRadius: '103%'
                    }]
            },
            // the value axis
            yAxis: {
                min: 0,
                max: 200,
                minorTickInterval: 'auto',
                minorTickWidth: 1,
                minorTickLength: 10,
                minorTickPosition: 'inside',
                minorTickColor: '#666',
                tickPixelInterval: 30,
                tickWidth: 2,
                tickPosition: 'inside',
                tickLength: 10,
                tickColor: '#666',
                labels: {
                    step: 2,
                    rotation: 'auto'
                },
                title: {
                    text: 'mb/s'
                },
                plotBands: [{
                        from: 0,
                        to: 120,
                        color: '#55BF3B' // green
                    }, {
                        from: 120,
                        to: 160,
                        color: '#DDDF0D' // yellow
                    }, {
                        from: 160,
                        to: 200,
                        color: '#DF5353' // red
                    }]
            },
            series: [{
                    name: 'Speed',
                    data: [80],
                    tooltip: {
                        valueSuffix: ' km/h'
                    }
                }]

        },
        // Add some life
        function(chart) {
            if (!chart.renderer.forExport) {
                setInterval(function() {
                    var point = chart.series[0].points[0],
                            newVal,
                            inc = Math.round((Math.random() - 0.5) * 20);

                    newVal = point.y + inc;
                    if (newVal < 0 || newVal > 200) {
                        newVal = point.y - inc;
                    }

                    point.update(newVal);

                }, 3000);
            }
        });
    });

</script>
<script>
    $(function() {
        $(document).ready(function() {
            Highcharts.setOptions({
                global: {
                    useUTC: false
                }
            });

            $('#chart3').highcharts({
                chart: {
                    type: 'spline',
                    animation: Highcharts.svg, // don't animate in old IE
                    marginRight: 10,
                    events: {
                        load: function() {

                            // set up the updating of the chart each second
                            var series = this.series[0];
                            setInterval(function() {
                                var x = (new Date()).getTime(), // current time
                                        y = Math.random();
                                series.addPoint([x, y], true, true);
                            }, 1000);
                        }
                    }
                },
                title: {
                    text: 'Live Signal Quality & Strength test'
                },
                xAxis: {
                    type: 'datetime',
                    tickPixelInterval: 150
                },
                yAxis: {
                    title: {
                        text: 'Value'
                    },
                    plotLines: [{
                            value: 0,
                            width: 1,
                            color: '#808080'
                        }]
                },
                tooltip: {
                    formatter: function() {
                        return '<b>' + this.series.name + '</b><br/>' +
                                Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', this.x) + '<br/>' +
                                Highcharts.numberFormat(this.y, 2);
                    }
                },
                legend: {
                    enabled: false
                },
                exporting: {
                    enabled: false
                },
                series: [{
                        name: 'Random data',
                        data: (function() {
                            // generate an array of random data
                            var data = [],
                                    time = (new Date()).getTime(),
                                    i;

                            for (i = -19; i <= 0; i += 1) {
                                data.push({
                                    x: time + i * 1000,
                                    y: Math.random()
                                });
                            }
                            return data;
                        }())
                    }]
            });
        });
    });
</script>

<script type="text/javascript">

    $(document).on('click', "#btn-success", function(evt) {
        evt.preventDefault();

        $('#alert').removeClass().addClass("alert alert-success").text("The incident has been logged succefully");
    });

</script>

<script>
    $(function() {
        $('.colors').hide();
        $('#model').change(function() {
            $('.colors').hide();
            $('#' + $(this).val()).show();
        });
    });
</script>




<div class="row">
    <h4>Sim View</h4>
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">

                <div class="col-lg-6">
                    <h4>Sim Details</h4>

                    <table class="table">

                        <tr class="active">	
                            <td><b><?php echo __('Sims number'); ?></b></td>

                            <td><?php echo h($sim['Sim']['id']); ?></td>
                        </tr>

                        <tr class="active">	
                            <td><b><?php echo __('Msisdn'); ?></b></td>

                            <td><?php echo h($sim['Sim']['msisdn']); ?></td>
                        </tr>

                        <tr class="active">	
                            <td><b><?php echo __('Iccid'); ?></b></td>

                            <td><?php echo h($sim['Sim']['iccid']); ?></td>
                        </tr>

                        <tr class="active">	
                            <td><b><?php echo __('IP Address'); ?></b></td>

                            <td><?php echo h($sim['Sim']['ip_address']); ?></td>
                        </tr>

                        <tr class="active">	



                            <!--<td><b><?php echo __('Status'); ?></b></td>-->
                            <!---<?php foreach ($status as $notification): ?>

                                <td><?php echo h($notification['Notification']['status']); ?></td>
                            <?php endforeach ?>--->
                        </tr>	



                        <tr class="active">	
                            <td><b><?php echo __('Customer'); ?></b></td>

                            <td><?php echo $this->Html->link($sim['Customer']['first_name'], array('controller' => 'customers', 'action' => 'view', $sim['Customer']['id'])); ?></td>
                        </tr>

                        <tr class="active">	
                            <td><b><?php echo __('Created'); ?></b></td>

                            <td><?php echo h($sim['Sim']['created']); ?></td>
                        </tr>


                    </table>

                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <h4>Sim Geolocation</h4>

                        <div id="map-canvas" style="height:300px"></div>
                        <script>

                            function initialize() {
                                var mapOptions = {
                                    zoom: 12,
                                    center: new google.maps.LatLng(<?php echo h($sim['Sim']['latitude']); ?>, <?php echo h($sim['Sim']['longitude']); ?>),
                                }
                                var map = new google.maps.Map(document.getElementById('map-canvas'),
                                        mapOptions);

                                var image = 'http://maps.google.com/mapfiles/ms/icons/red-dot.png';
                                var myLatLng = new google.maps.LatLng(<?php echo h($sim['Sim']['latitude']); ?>, <?php echo h($sim['Sim']['longitude']); ?>);
                                var beachMarker = new google.maps.Marker({
                                    position: myLatLng,
                                    map: map,
                                    icon: image
                                });
                            }

                            google.maps.event.addDomListener(window, 'load', initialize);

                        </script>


                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">

    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">

                       <div class="incidents form">
					   <h4>Add Incident</h4>
                        <?php echo $this->Form->create('Incident'); ?>
                        <fieldset>
                            <div class="col-lg-6">
                               
                                <?php echo $this->Form->input('description', array('class' => "form-control")); ?>
                                <?php echo $this->Form->input('incident_type_id', array('class' => "form-control")); ?>
                                <?php echo $this->Form->input('severity_id', array('class' => "form-control")); ?>
                                <?php echo $this->Form->input('incident_status_id', array('class' => "form-control")); ?>
                            </div>
                            <div class="col-lg-6">
                                <?php echo $this->Form->input('technician_id', array('class' => "form-control")); ?>
                                <?php echo $this->Form->input('user_id', array('class' => "form-control")); ?>
                                <?php echo $this->Form->input('parent_id', array('class' => "form-control")); ?>
                                <?php echo $this->Form->input('Sim_id', array('disabled' => true, 'class' => "form-control")); ?>
	
								
								</br>
									<?php
									echo $this->Form->submit('Submit', array(
										'div' => 'form-group',
										'class' => 'btn btn-primary'
									));
									?>
                            </div>
                        </fieldset>
                        
                    </div>
                
            </div>
        </div>
    </div>



</div>



<div class="row">

    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
			<h4>Incident History</h4>
			
</div>	
	</div>
		</div>
			</div>
</div>			
