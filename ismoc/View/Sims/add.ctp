<div class="sims form">
    <?php echo $this->Form->create('Sim'); ?>
    <fieldset>
        <legend><?php echo __('Add Sim'); ?></legend>
        <?php
        echo $this->Form->input('msisdn');
        echo $this->Form->input('iccid');
        echo $this->Form->input('ip_address');
        echo $this->Form->input('longitude');
        echo $this->Form->input('latitude');
        echo $this->Form->input('customer_id');
        echo $this->Form->input('modifeid');
        ?>
    </fieldset>
    <?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
    <h3><?php echo __('Actions'); ?></h3>
    <ul>

        <li><?php echo $this->Html->link(__('List Sims'), array('action' => 'index')); ?></li>
        <li><?php echo $this->Html->link(__('List Apns'), array('controller' => 'apns', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('New Apn'), array('controller' => 'apns', 'action' => 'add')); ?> </li>
        <li><?php echo $this->Html->link(__('List Customers'), array('controller' => 'customers', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('New Customer'), array('controller' => 'customers', 'action' => 'add')); ?> </li>
        <li><?php echo $this->Html->link(__('List Notifications'), array('controller' => 'notifications', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('New Notification'), array('controller' => 'notifications', 'action' => 'add')); ?> </li>
        <li><?php echo $this->Html->link(__('List Incidents'), array('controller' => 'incidents', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('New Incident'), array('controller' => 'incidents', 'action' => 'add')); ?> </li>
    </ul>
</div>
