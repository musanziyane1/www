<?php echo $this->Html->addCrumb("Incidents Overview", "/incidents/overview"); ?>
<?php echo $this->Html->addCrumb("View"); ?>
    <div class="row">
     <h3>Incident View</h3>
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="col-lg-6">
                        <h4>Incident Details</h4>

                        <br /> 

                        <table class="table">
                            <tr class="active">
                                <td><b>Incident Number</b></td>
                                <td><?php echo h($incident['Incident']['id']); ?></td>
                            </tr>

                            <tr class="active">
                                <th><b>Parent Incident</th>
                                <td><?php echo $this->Html->link($incident['ParentIncident']['description'], array('controller' => 'incidents', 'action' => 'view', $incident['ParentIncident']['id'])); ?>
                                </td>

                            </tr>

                            <tr class="active"> 
                                <td><b>Decription</b></td>
                                <td><?php echo h($incident['Incident']['description']); ?></td>
                            </tr>
                            <tr class="active"> 
                                <td><b>Type</b></td>
                                <td><?php echo $this->Html->link($incident['IncidentType']['description'], array('controller' => 'incident_types', 'action' => 'view', $incident['IncidentType']['id'])); ?></td>
                            </tr>

                            <tr class="active">
                                <td><b>Severity</b></td>
                                <td><?php echo $this->Html->link($incident['Severity']['description'], array('controller' => 'severities', 'action' => 'view', $incident['Severity']['id'])); ?></td>
                            </tr>

                            <tr class="active">
                                <td><b>Status</b></td>
                                <td>	
                                    <?php echo $this->Html->link($incident['IncidentStatus']['description'], array('controller' => 'incident_statuses', 'action' => 'view', $incident['IncidentStatus']['id'])); ?>
                                </td>
                            </tr>

                            <tr class="active">
                                <td><b>Logged By</b></td>
                                <td><?php echo $this->Html->link($incident['User']['first_name'], array('controller' => 'users', 'action' => 'view', $incident['User']['id'])); ?>
                                </td>
                            </tr>

                            <tr class="active">
                                <td><b>Technician Assigned</b></td>
                                <td>
                                    <?php echo $this->Html->link($incident['Technician']['first_name'], array('controller' => 'technicians', 'action' => 'view', $incident['Technician']['id'])); ?>
                                </td>
                            </tr>

                            <tr class="active">
                                <td><b>Logged Date</b></td>
                                <td><?php echo h($incident['Incident']['created']); ?></td>
                            </tr>

                            <tr class="active">
                                <td><b>Date Modified</b></td>
                                <td><?php echo h($incident['Incident']['modified']); ?></td>
                            </tr>



                            <tr class="active">
                                <th><b>Sim Number</th>
                                <td><a href="Sim_View.html">00001</a></td>

                            </tr>
                        </table>


                    </div>

                    <div class="col-lg-6">

                        <h4>Incident Geolocation</h4></br>
                        <div class="panel panel-default">




                            <div id="map-canvas" style="height:400px"></div>
                            <script>
                                // This example adds a marker to indicate the position
                                // of Bondi Beach in Sydney, Australia
                                function initialize() {
                                    var mapOptions = {
                                        zoom: 8,
                                        center: new google.maps.LatLng(-25.8573, 28.1894),
                                    }
                                    var map = new google.maps.Map(document.getElementById('map-canvas'),
                                            mapOptions);

                                    var image = 'http://maps.google.com/mapfiles/ms/icons/red-dot.png';
                                    var myLatLng = new google.maps.LatLng(-25.8573, 28.1894);
                                    var beachMarker = new google.maps.Marker({
                                        position: myLatLng,
                                        map: map,
                                        icon: image
                                    });
                                }

                                google.maps.event.addDomListener(window, 'load', initialize);

                            </script>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="incidents form">
                        <?php echo $this->Form->create('Incident'); ?>
                        <fieldset>
                            <legend><?php echo __('Edit Incident'); ?></legend>
                            <div class="col-lg-6">
                                <?php echo $this->Form->input('id'); ?>
                                <?php echo $this->Form->input('description', array('class' => "form-control")); ?>
                                <?php echo $this->Form->input('incident_type_id', array('class' => "form-control")); ?>
                                <?php echo $this->Form->input('severity_id', array('class' => "form-control")); ?>

                                <?php echo $this->Form->input('incident_status_id', array('class' => "form-control")); ?>
                            </div>
                            <div class="col-lg-6">
                                <?php echo $this->Form->input('technician_id', array('class' => "form-control")); ?>
                                <?php echo $this->Form->input('user_id', array('class' => "form-control")); ?>
                                <?php echo $this->Form->input('parent_id', array('class' => "form-control")); ?>
                                <?php echo $this->Form->input('Sim', array('class' => "form-control")); ?>
                            </div>
                        </fieldset>
                        </br>
                        <?php
                        echo $this->Form->submit('Submit', array(
                            'div' => 'form-group',
                            'class' => 'btn btn-primary'
                        ));
                        ?>
                    </div>

                </div>
            </div>
        </div>
    </div>



    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="related">
                        <h4><?php echo __('Incident Updates'); ?></h4>
                        <?php if (!empty($incident['IncidentUpdate'])): ?>
                            <div class='table-responsive'>
                                <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <td><?php echo __('Id'); ?></td>
                                            <td><?php echo __('Description'); ?></td>
                                            <td><?php echo __('Type'); ?></td>
                                            <td><?php echo __('Severity'); ?></td>
                                            <td><?php echo __('Status'); ?></td>
                                            <td><?php echo __('Technician'); ?></td>
                                            <td><?php echo __('Updated By'); ?></td>
                                            <td><?php echo __('Created'); ?></td>
                                            <td><?php echo __('Modified'); ?></td>
                                            <td><?php echo __('Authorised By'); ?></td>


                                        </tr>
                                    </thead>
                                    <?php foreach ($incident['IncidentUpdate'] as $incidentUpdate): ?>
                                        <tr>
                                            <td><?php echo $incidentUpdate['id']; ?></td>
                                            <td><?php echo $incidentUpdate['description']; ?></td>
                                            <td><?php echo $incidentUpdate['incident_type_id']; ?></td>
                                            <td><?php echo $incidentUpdate['severity_id']; ?></td>
                                            <td><?php echo $incidentUpdate['incident_status_id']; ?></td>
                                            <td><?php echo $incidentUpdate['technician_id']; ?></td>
                                            <td><?php echo $incidentUpdate['user_id']; ?></td>
                                            <td><?php echo $incidentUpdate['created']; ?></td>
                                            <td><?php echo $incidentUpdate['modified']; ?></td>
                                            <td><?php echo $incidentUpdate['authorised_by']; ?></td>

                                        </tr>
                                    <?php endforeach; ?>
                                </table>
                            </div>
                        <?php endif; ?>


                    </div>

                </div>


            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="related">
                        <h3><?php echo __('Related Incidents'); ?></h3>
                        <?php if (!empty($incident['IncidentUpdate'])): ?>
                            <div class='table-responsive'>
                                <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <td><?php echo __('Id'); ?></td>
                                            <td><?php echo __('Description'); ?></td>
                                            <td><?php echo __('Incident Type Id'); ?></td>
                                            <td><?php echo __('Severity Id'); ?></td>
                                            <td><?php echo __('Incident Status Id'); ?></td>
                                            <td><?php echo __('Technician Id'); ?></td>
                                            <td><?php echo __('User Id'); ?></td>
                                            <td><?php echo __('Parent Id'); ?></td>
                                            <td><?php echo __('Created'); ?></td>
                                            <td><?php echo __('Modified'); ?></td>
                                            <td class="actions"><?php echo __('Actions'); ?></td>
                                        </tr>
                                    </thead>
                                    <?php foreach ($incident['ChildIncident'] as $childIncident): ?>
                                        <tr>
                                            <td><?php echo $childIncident['id']; ?></td>
                                            <td><?php echo $childIncident['description']; ?></td>
                                            <td><?php echo $childIncident['incident_type_id']; ?></td>
                                            <td><?php echo $childIncident['severity_id']; ?></td>
                                            <td><?php echo $childIncident['incident_status_id']; ?></td>
                                            <td><?php echo $childIncident['technician_id']; ?></td>
                                            <td><?php echo $childIncident['user_id']; ?></td>
                                            <td><?php echo $childIncident['parent_id']; ?></td>
                                            <td><?php echo $childIncident['created']; ?></td>
                                            <td><?php echo $childIncident['modified']; ?></td>
                                            <td class="actions">
                                            <?php echo $this->Html->link(__('View'), array('controller' => 'incidents', 'action' => 'view', $childIncident['id'])); ?>

                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </table>
                            </div>
                        <?php endif; ?>

  </div>
</div>
        </div>
</div>


