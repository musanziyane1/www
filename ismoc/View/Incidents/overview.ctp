<?php echo $this->Html->addCrumb("Incidents Overview"); ?>

<style>
    .navbar a.navbar-brand {padding: 2px 15px 2px; }
</style>

<script>
    $(document).ready(function() {
        $('#data-table').dataTable();
    });
</script>


<script>
    $(function() {
        $('.colors').hide();
        $('#model').change(function() {
            $('.colors').hide();
            $('#' + $(this).val()).show();
        });
    });
</script>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">

                    <h4>Incidents Overview</h4>



                    <div class='table-responsive'>
                        <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">

                            <thead>
                                <tr>
                                    <th><?php echo $this->Paginator->sort('description'); ?></th>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </thead>

                            <thead>
                                <tr>
                                    <td><?php echo '<b>Description</b>'; ?></td>
                                    <td><?php echo '<b>Incident Types</b>'; ?></td>
                                    <td><?php echo '<b>Severity</b>'; ?></td>
                                    <td><?php echo '<b>Incident Status</b>'; ?></td>
                                    <td><?php echo '<b>Actions</b>'; ?></td>

                                </tr>
                            </thead>
                            <tbody>

                                <?php foreach ($incidents as $incident): ?>
                                    <tr>

                                        <td><?php echo h($incident['Incident']['description']); ?>&nbsp;</td>

                                        <td>
                                            <?php echo $this->Html->link($incident['IncidentType']['description'], array('controller' => 'incident_types', 'action' => 'view', $incident['IncidentType']['id'])); ?>
                                        </td>
                                        <td>
                                            <?php echo $this->Html->link($incident['Severity']['id'], array('controller' => 'severities', 'action' => 'view', $incident['Severity']['id'])); ?>
                                        </td>
                                        <td>
                                            <?php echo $this->Html->link($incident['IncidentStatus']['description'], array('controller' => 'incident_statuses', 'action' => 'view', $incident['IncidentStatus']['id'])); ?>
                                        </td>
                                        <td>
                                            <?php echo $this->Html->link(__('View'), array('action' => 'view', $incident['Incident']['id'])); ?>
                                        </td>


                                    </tr>
                                <?php endforeach; ?>

                            </tbody>	

                    </div>


                </div>
            </div>
        </div>

    </div>