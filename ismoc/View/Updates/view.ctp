<div class="updates view">
<h2><?php echo __('Update'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($update['Update']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Incident'); ?></dt>
		<dd>
			<?php echo $this->Html->link($update['Incident']['title'], array('controller' => 'incidents', 'action' => 'view', $update['Incident']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Description'); ?></dt>
		<dd>
			<?php echo h($update['Update']['description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Date Updated'); ?></dt>
		<dd>
			<?php echo h($update['Update']['date_updated']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Change Description'); ?></dt>
		<dd>
			<?php echo h($update['Update']['change_description']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Update'), array('action' => 'edit', $update['Update']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Update'), array('action' => 'delete', $update['Update']['id']), array(), __('Are you sure you want to delete # %s?', $update['Update']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Updates'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Update'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Incidents'), array('controller' => 'incidents', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Incident'), array('controller' => 'incidents', 'action' => 'add')); ?> </li>
	</ul>
</div>
