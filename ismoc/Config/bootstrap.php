<?php

/**
 * This file is loaded automatically by the app/webroot/index.php file after core.php
 *
 * This file should load/create any application wide configuration settings, such as
 * Caching, Logging, loading additional configuration files.
 *
 * You should also use this file to include any files that provide global functions/constants
 * that your application uses.
 *
 * PHP 5
 *
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.10.8.2117
 */
// Setup a 'default' cache configuration for use in the application.
Cache::config('default', array('engine' => 'File'));

/**
 * The settings below can be used to set additional paths to models, views and controllers.
 *
 * App::build(array(
 *     'Model'                     => array('/path/to/models/', '/next/path/to/models/'),
 *     'Model/Behavior'            => array('/path/to/behaviors/', '/next/path/to/behaviors/'),
 *     'Model/Datasource'          => array('/path/to/datasources/', '/next/path/to/datasources/'),
 *     'Model/Datasource/Database' => array('/path/to/databases/', '/next/path/to/database/'),
 *     'Model/Datasource/Session'  => array('/path/to/sessions/', '/next/path/to/sessions/'),
 *     'Controller'                => array('/path/to/controllers/', '/next/path/to/controllers/'),
 *     'Controller/Component'      => array('/path/to/components/', '/next/path/to/components/'),
 *     'Controller/Component/Auth' => array('/path/to/auths/', '/next/path/to/auths/'),
 *     'Controller/Component/Acl'  => array('/path/to/acls/', '/next/path/to/acls/'),
 *     'View'                      => array('/path/to/views/', '/next/path/to/views/'),
 *     'View/Helper'               => array('/path/to/helpers/', '/next/path/to/helpers/'),
 *     'Console'                   => array('/path/to/consoles/', '/next/path/to/consoles/'),
 *     'Console/Command'           => array('/path/to/commands/', '/next/path/to/commands/'),
 *     'Console/Command/Task'      => array('/path/to/tasks/', '/next/path/to/tasks/'),
 *     'Lib'                       => array('/path/to/libs/', '/next/path/to/libs/'),
 *     'Locale'                    => array('/path/to/locales/', '/next/path/to/locales/'),
 *     'Vendor'                    => array('/path/to/vendors/', '/next/path/to/vendors/'),
 *     'Plugin'                    => array('/path/to/plugins/', '/next/path/to/plugins/'),
 * ));
 *
 */
/**
 * Custom Inflector rules, can be set to correctly pluralize or singularize table, model, controller names or whatever other
 * string is passed to the inflection functions
 *
 * Inflector::rules('singular', array('rules' => array(), 'irregular' => array(), 'uninflected' => array()));
 * Inflector::rules('plural', array('rules' => array(), 'irregular' => array(), 'uninflected' => array()));
 *
 */
/**
 * Plugins need to be loaded manually, you can either load them one by one or all of them in a single call
 * Uncomment one of the lines below, as you need. make sure you read the documentation on CakePlugin to use more
 * advanced ways of loading plugins
 *
 * CakePlugin::loadAll(); // Loads all plugins at once
 * CakePlugin::load('DebugKit'); //Loads a single plugin named DebugKit
 *
 */
/**
 * You can attach event listeners to the request lifecycle as Dispatcher Filter . By Default CakePHP bundles two filters:
 *
 * - AssetDispatcher filter will serve your asset files (css, images, js, etc) from your themes and plugins
 * - CacheDispatcher filter will read the Cache.check configure variable and try to serve cached content generated from controllers
 *
 * Feel free to remove or add filters as you see fit for your application. A few examples:
 *
 * Configure::write('Dispatcher.filters', array(
 * 		'MyCacheFilter', //  will use MyCacheFilter class from the Routing/Filter package in your app.
 * 		'MyPlugin.MyFilter', // will use MyFilter class from the Routing/Filter package in MyPlugin plugin.
 * 		array('callable' => $aFunction, 'on' => 'before', 'priority' => 9), // A valid PHP callback type to be called on beforeDispatch
 * 		array('callable' => $anotherMethod, 'on' => 'after'), // A valid PHP callback type to be called on afterDispatch
 *
 * ));
 */
Configure::write('Dispatcher.filters', array(
    'AssetDispatcher',
    'CacheDispatcher'
));

Configure::write('App.varOne', array('someKey' => 'someValue'));
/**
 * Configures default file logging options
 */
App::uses('CakeLog', 'Log');
CakeLog::config('debug', array(
    'engine' => 'File',
    'types' => array('notice', 'info', 'debug'),
    'file' => 'debug',
));
CakeLog::config('error', array(
    'engine' => 'File',
    'types' => array('warning', 'error', 'critical', 'alert', 'emergency'),
    'file' => 'error',
));

configure::write('myDevices', array(
    'Device1' => array(
        'msisdn' => '27781003888',
        'imsi' => '655103041517618',
        'ext' => '-11',
        'phone_number' => '27810480511',
        'class_name' => 'Device',
        'activated' => '2014-01-24',
        'longitude' => 26.593356,
        'latitude' => -26.912489,
        'communication_state' => 0,
        'imei' => '123409565869201',
        'operating_color' => 'blue',
        'id' => 18,
        'cached_state_label' => 'Comms Error',
        'cached_state_color' => 'orangered',
        'state' => 'ACTIVATED',
        'customer_model_id' => 509,
        'serial_number' => '123409565869201',
        'folder' => 'Electricity Metering',
        'customer_id' => 5672617,
        'operating_state' => 'None',
        'model_id' => 509,
        'registered' => '2014-01-20',
        'ip_address' => '',
        'name' => 'Test Device Building A',
        'created' => '2014-01-20T14:28:27',
        'communication_color' => 'red',
        'modified' => '2014-01-24T14:05:09',
        'unique_id' => '123409565869201',
        'folder_id' => 5672619
    ),
    'Device2' => array(
        'msisdn' => '27810480511',
        'imsi' => '655103189096240',
        'ext' => '8',
        'operating_state' => 'None',
        'class_name' => 'Device',
        'activated' => '2013-10-31',
        'communication_state' => 1,
        'operating_color' => 'blue',
        'latitude' => -27.683741,
        'longitude' => 29.977144,
        'id' => 22,
        'description' => 'Installed at Martins house',
        'metric_model_id' => 407,
        'cached_state_color' => 'lime',
        'customer_model_id' => 509,
        'serial_number' => '1234123456781',
        'folder' => 'Martins house',
        'customer_id' => 5672617,
        'phone_number' => '27810480512',
        'model_id' => 509,
        'cached_state_label' => 'Comms OK',
        'state' => 'ACTIVATED',
        'registered' => '2013-10-31',
        'imei' => '123409565869200',
        'ip_address' => '',
        'name' => 'Test Device Building B',
        'created' => '2013-10-31T15:04:58',
        'communication_color' => 'green',
        'modified' => '2014-07-20T19:02:02',
        'metric_model' => 'ModemMetricModel',
        'communication_date' => '2014-07-24T11:47:09',
        'unique_id' => '123409565869200',
        'folder_id' => 5764272,
    ),
    'Device3' => array(
        'msisdn' => '27781003883',
        'imsi' => '655103041517613',
        'ext' => '-5',
        'operating_state' => 'None',
        'class_name' => 'Device',
        'activated' => '2014-03-12',
        'communication_state' => 0,
        'longitude' => 24.813571,
        'latitude' => -28.550963,
        'operating_color' => 'blue',
        'id' => 22,
        'description' => 'Demo unit fitted in travel case for Road Shows.',
        'state' => 'ACTIVATED',
        'cached_state_color' => 'orangered',
        'customer_model_id' => 509,
        'serial_number' => '123415242647100',
        'folder' => 'Electricity Metering',
        'customer_id' => 5672617,
        'phone_number' => '27810480231',
        'model_id' => 509,
        'cached_state_label' => 'Comms Error',
        'registered' => '2014-03-11',
        'imei' => '123415242647100',
        'ip_address' => '',
        'name' => 'Test Device Building C',
        'created' => '2014-03-11T09:35:24',
        'communication_color' => 'red',
        'modified' => '2014-06-04T15:23:57',
        'communication_date' => '2014-06-04T14:53:13',
        'unique_id' => '123415242647100',
        'folder_id' => 5672619,
    ),
    'Device4' => array(
        'msisdn' => '27810480514',
        'imsi' => '655103189096244',
        'ext' => '-1',
        'phone_number' => '27810480514',
        'class_name' => 'Device',
        'activated' => '2014-01-24',
        'communication_state' => 0,
        'longitude' => 28.097014,
        'latitude' => -26.102940,
        'imei' => '123409565869204',
        'operating_color' => 'blue',
        'id' => 4,
        'cached_state_label' => 'Comms Error',
        'cached_state_color' => 'orangered',
        'state' => 'ACTIVATED',
        'customer_model_id' => 504,
        'serial_number' => '123409565869204',
        'folder' => 'Electricity Metering',
        'customer_id' => 5672614,
        'operating_state' => 'None',
        'model_id' => 504,
        'registered' => '2014-01-20',
        'ip_address' => '',
        'name' => 'Test Device Building D',
        'created' => '2014-01-20T14:28:27',
        'communication_color' => 'red',
        'modified' => '2014-01-24T14:05:09',
        'unique_id' => '123409565869204',
        'folder_id' => 5672619
    ),
    'Device5' => array(
        'msisdn' => '27781003885',
        'imsi' => '655103041517615',
        'ext' => '2',
        'operating_state' => 'None',
        'pin' => '',
        'class_name' => 'Device',
        'activated' => '2013-10-31',
        'communication_state' => 1,
        'operating_color' => 'blue',
        'longitude' => 29.435703,
        'latitude' => -23.912915,
        'id' => 25,
        'description' => 'Installed at Martins house',
        'metric_model_id' => 407,
        'cached_state_color' => 'lime',
        'customer_model_id' => 509,
        'serial_number' => '1234123456785',
        'folder' => 'Martins house',
        'customer_id' => 5672615,
        'phone_number' => '27810480515',
        'model_id' => 505,
        'cached_state_label' => 'Comms OK',
        'state' => 'ACTIVATED',
        'registered' => '2013-10-31',
        'imei' => '123409565869205',
        'ip_address' => '',
        'name' => 'Test Device Building E',
        'created' => '2013-10-31T15:04:58',
        'communication_color' => 'green',
        'modified' => '2014-07-20T19:02:02',
        'metric_model' => 'ModemMetricModel',
        'communication_date' => '2014-07-24T11:47:09',
        'unique_id' => '123409565869205',
        'folder_id' => 5764272,
    ),
));

configure::write('mySims', array(
    'Sim1' => array(
        'phone_number' => '27810480511',
        'status' => 'Active',
        'is_deleted' => 'false',
        'name' => 'Test Sim Building A',
        'folder_name' => 'Electricity Metering',
        'created' => '2013-11-14T09:36:06',
        'class_name' => 'Sim',
        'apn_name' => 'apn.trintel.co.za (MTN)',
        'modified' => '2014-07-24T10:41:42',
        'msisdn' => '27781003888',
        'folder_id' => 5672619,
        'is_online' => 'false',
        'iccid' => '8927000000853857593',
        'customer_id' => 5672617,
        'mtd_usage' => 0,
        'id' => 49375,
        'imsi' => '655103041517618',
        'customer_name' => 'Electricity Metering',
        'ip_address' => '192.168.0.0'
    ),
    'Sim2' => array(
        'phone_number' => '27810480512',
        'status' => 'Active',
        'name' => 'Test Sim Building B',
        'folder_name' => 'Electricity Metering',
        'created' => '2014-01-23T14:01:41',
        'class_name' => 'Sim',
        'apn_name' => 'apn.trintel.co.za (MTN)',
        'modified' => '2014-07-24T03:42:38',
        'msisdn' => '27810480511',
        'folder_id' => 5672620,
        'is_online' => 'false',
        'iccid' => '8927000002142174343',
        'customer_id' => '5672617',
        'mtd_usage' => '0',
        'id' => '53543',
        'imsi' => '655103189096240',
        'customer_name' => 'Electricity Metering',
        'ip_address' => '192.168.1.1'
    ),
    'Sim3' => array(
        'phone_number' => '27810480231',
        'status' => 'Active',
        'is_deleted' => 'false',
        'name' => 'Test Sim building C',
        'folder_name' => 'Electricity Metering',
        'created' => '2013-11-14T09:36:06',
        'class_name' => 'Sim',
        'apn_name' => 'apn.trintel.co.za (MTN)',
        'modified' => '2014-07-24T10:41:42',
        'msisdn' => '27781003883',
        'folder_id' => 5672613,
        'is_online' => 'false',
        'iccid' => '8927000000853857533',
        'customer_id' => 5672617,
        'mtd_usage' => 0,
        'id' => 49373,
        'imsi' => '655103041517613',
        'customer_name' => 'Electricity Metering',
        'ip_address' => '192.168.2.2'
    ),
    'Sim4' => array(
        'phone_number' => '27810480514',
        'status' => 'Active',
        'is_deleted' => 'false',
        'name' => 'Test Sim Building D',
        'folder_name' => 'Electricity Metering',
        'created' => '2014-01-23T14:01:41',
        'class_name' => 'Sim',
        'apn_name' => 'apn.trintel.co.za (MTN)',
        'modified' => '2014-07-24T03:42:38',
        'msisdn' => '27810480514',
        'folder_id' => 5672614,
        'is_online' => 'false',
        'iccid' => '8927000002142174344',
        'customer_id' => '5672614',
        'mtd_usage' => '0',
        'id' => '53544',
        'imsi' => '655103189096244',
        'customer_name' => 'Electricity Metering',
        'ip_address' => '192.168.3.1'
    ),
    'Sim5' => array(
        'phone_number' => '27810480515',
        'status' => 'Active',
        'is_deleted' => 'false',
        'name' => 'Test Sim Building E',
        'folder_name' => 'Electricity Metering',
        'created' => '2013-11-14T09:36:06',
        'class_name' => 'Sim',
        'apn_name' => 'apn.trintel.co.za (MTN)',
        'modified' => '2014-07-24T10:41:42',
        'msisdn' => '27781003885',
        'folder_id' => 5672615,
        'is_online' => 'false',
        'iccid' => '8927000000853857595',
        'customer_id' => 5672615,
        'mtd_usage' => 0,
        'id' => 49355,
        'imsi' => '655103041517615',
        'customer_name' => 'Electricity Metering',
        'ip_address' => '192.168.3.8'
    )
));

Configure::write('myAssets', array(
    'Asset1' => array(
        'communication_state' => 0,
        'device' => '123409565869201',
        'metric_model' => 'A1140 metric model',
        'operating_color' => 'blue',
        'id' => 25061,
        'metric_model_id' => 361,
        'asset_model_id' => 511,
        'cached_state_color' => 'orangered',
        'operating_state' => 'None',
        'cached_state_label' => 'Comms Error',
        'device_id' => 57651,
        'name' => 'Test Meter at Building A',
        'communication_color' => 'red',
        'external_id' => 'A1140DC',
        'device_id' => 50401
    ),
    'Asset2' => array(
        'communication_state' => 0,
        'device' => '123409565869202',
        'metric_model' => 'A1140 metric model',
        'operating_color' => 'blue',
        'id' => 25062,
        'metric_model_id' => 362,
        'asset_model_id' => 513,
        'cached_state_color' => 'orangered',
        'operating_state' => 'None',
        'cached_state_label' => 'Comms Error',
        'device_id' => 57652,
        'name' => 'Test Meter at Building B',
        'communication_color' => 'red',
        'external_id' => 'A1140DC',
        'device_id' => 50404
    ),
    'Asset3' => array(
        'communication_state' => 0,
        'device' => '123409565869203',
        'metric_model' => 'A1140 metric model',
        'operating_color' => 'blue',
        'id' => 25063,
        'metric_model_id' => 363,
        'asset_model_id' => 513,
        'cached_state_color' => 'orangered',
        'operating_state' => 'None',
        'cached_state_label' => 'Comms Error',
        'device_id' => 57653,
        'name' => 'Test Meter at Building C',
        'communication_color' => 'red',
        'external_id' => 'A1140DC',
        'device_id' => 50403
    ),
    'Asset4' => array(
        'communication_state' => 0,
        'device' => '123409565869204',
        'metric_model' => 'A1140 metric model',
        'operating_color' => 'blue',
        'id' => 25064,
        'metric_model_id' => 364,
        'asset_model_id' => 514,
        'cached_state_color' => 'orangered',
        'operating_state' => 'None',
        'cached_state_label' => 'Comms Error',
        'device_id' => 57654,
        'name' => 'Test Meter at Building D',
        'communication_color' => 'red',
        'external_id' => 'A1140DC',
        'device_id' => 50404
    ),
    'Asset5' => array(
        'communication_state' => 0,
        'device' => '123409565869205',
        'metric_model' => 'A1140 metric model',
        'operating_color' => 'blue',
        'id' => 25065,
        'metric_model_id' => 365,
        'asset_model_id' => 515,
        'cached_state_color' => 'orangered',
        'operating_state' => 'None',
        'cached_state_label' => 'Comms Error',
        'device_id' => 57655,
        'name' => 'Test Meter at Building E',
        'communication_color' => 'red',
        'external_id' => 'A1140DC',
        'device_id' => 50405
    ),
));

CakePlugin::loadAll();

